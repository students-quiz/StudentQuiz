## Student Quiz: Privacy policy

Welcome to the Student Quiz app for Android!

This is an open source Android app developed by Arnab Senapati. The source code is available on GitLab under the MIT license; the app is also available on Google Play.

As an avid Android user myself, I take privacy very seriously.
I know how irritating it is when apps collect your data without your knowledge.

I have not programmed this app to collect any personally identifiable information. All data (app preferences (like theme, etc.) ) created by the you is stored on your device only, and can be simply erased by clearing the app's data or uninstalling it. Email entered during login is saved in database for authorization

If you find any security vulnerability that has been inadvertently caused by me, or have any question regarding how the app protectes your privacy, please send me an email and I will surely try to fix it/help you.

Yours sincerely,  
Arnab Senapati.  
Kolkata, India.  
senapati.arnab@gmail.com
