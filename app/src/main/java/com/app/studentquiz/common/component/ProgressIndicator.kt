package com.app.studentquiz.common.component

import androidx.compose.animation.animateColor
import androidx.compose.animation.core.*
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import com.app.studentquiz.R
import com.app.studentquiz.ui.theme.blue200


@Composable
fun IndefiniteProgressIndicator(){
    val infiniteTransition = rememberInfiniteTransition()
    val angle by infiniteTransition.animateFloat(
        initialValue = 0f,
        targetValue = 360f,
        animationSpec = infiniteRepeatable(
            animation = tween(1000, easing = LinearEasing),
            repeatMode = RepeatMode.Restart
        )
    )

    Box(
        Modifier
            .background(Color.White)){
        Image(
            painter = painterResource(id = R.drawable.reward) ,
            contentDescription = "Progress",
            modifier = Modifier.rotate(angle)
        )
    }
}

@Preview(showBackground = true)
@Composable
fun ProgressPreview(){
    IndefiniteProgressIndicator()
}