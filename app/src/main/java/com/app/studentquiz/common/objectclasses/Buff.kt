package com.app.studentquiz.common.objectclasses

data class Buff(
    val name: String = "",
    val image_url: String ="",
    val displayname: String=""

)
