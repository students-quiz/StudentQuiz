package com.app.studentquiz.common.component

import androidx.compose.runtime.Composable
import androidx.compose.ui.viewinterop.AndroidView
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView

@Composable
fun Youtube(
    videoId: String
){
    AndroidView(factory = { context ->
        YouTubePlayerView(context).apply {
            addYouTubePlayerListener(YoutubePlayerListener(videoId))
        }
    })
}

class YoutubePlayerListener(
    private val videoId: String
) : AbstractYouTubePlayerListener() {
    override fun onReady(youTubePlayer: YouTubePlayer) {
        val videoId = videoId
        youTubePlayer.loadVideo(videoId, 0F)
    }
}