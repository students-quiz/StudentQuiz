package com.app.studentquiz.common.objectclasses

data class Reward(
    val name: String = "",
    val image_url: String ="",
    val min_encash: Double=99999.0,
    val weight: Double=0.0

)
