package com.app.studentquiz.screens.questions

import androidx.compose.runtime.Stable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue

@Stable
class QuestionState(
    val id: String,
    val questionData: Question,
    val questionIndex: Int,
    var totalQuestionsCount: Int,
    val showPrevious: Boolean,
    var showDone: Boolean
) {
    var answer by mutableStateOf(-1)
    var enableNext by mutableStateOf(true)
    var submitted by mutableStateOf(false)
    var bonusAdded by mutableStateOf(false)
}