package com.app.studentquiz.screens

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import com.app.studentquiz.repo.users.User
import com.app.studentquiz.screens.login.LoginRepository
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

@HiltViewModel
class BottomNavViewModel @Inject constructor(

    val db: FirebaseFirestore,
    private val authRepo: LoginRepository,
) : ViewModel() {
    var isTeacher by mutableStateOf(false)

    /**
     *
     * does user has teacher role
     */
    fun checkRole(){
        val firebaseUser = authRepo.currentUser()
        firebaseUser?.apply {
            db.collection("users")
                .document(uid)// firebaseuser.uid
                .get()
                .addOnSuccessListener { ds ->
                    ds?.let {
                        val user = ds.toObject(User::class.java)!!
                        val tcR = user.roles["teacher"]
                        tcR?.let { isTeacher = it }
                    }
                }
        }
    }
}