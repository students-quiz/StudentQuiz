@file:OptIn(ExperimentalMaterialApi::class)

package com.app.studentquiz.screens.result

import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.app.studentquiz.charts.pie.PieChart
import com.app.studentquiz.charts.pie.PieChartData
import com.app.studentquiz.charts.pie.SimpleSliceDrawer
import com.app.studentquiz.charts.simpleChartAnimation
import com.app.studentquiz.common.component.IndefiniteProgressIndicator
import com.app.studentquiz.common.component.NetworkImage
import com.app.studentquiz.common.objectclasses.Buff
import com.app.studentquiz.common.objectclasses.Reward
import com.app.studentquiz.screens.questions.Question
import com.app.studentquiz.screens.questions.QuestionState
import com.google.accompanist.flowlayout.FlowRow

@Composable
fun ResultScreen(
    questionState: List<QuestionState>?,
    vm: ResultScreenViewModel,
    navController: NavController
) {
    questionState?.let {
        LaunchedEffect(key1 = questionState) {
            vm.calcScore(questionState)
        }
    }

    ResultContent(
        questionState,
        //Calculation in progress when either of these are in progress
        vm.buffCalcInProgress.value || vm.rewardCalcInProgress.value || vm.bonusCalcInProgress.value,
        vm.resultCalcFailed.value,
        vm.score.value,
        vm.bonusScore.value,//bonus score (Score object)
        vm.buff.value,
        vm.reward.value,
        vm.bonusReward.value// bonus reward (Reward object)
    )

}


@Composable
private fun ResultContent(
    questionState: List<QuestionState>?,
    calcInProgress: Boolean,
    calcFailed: Boolean,
    score: Score,
    bonusScore: Score,
    bf: Buff,
    rwd: Reward,
    bonRwd: Reward
) {
    questionState?.let {

        val totalQuestion = questionState.size
        val correctQuestions = questionState.filter {
            it.answer == it.questionData.correct_option
        }
        val correctNumber = correctQuestions.size
        Scaffold {
            LazyColumn(
                contentPadding = PaddingValues(start = 20.dp, end = 20.dp)
            ) {
                item {
                    Card(
                        modifier = Modifier
                            .padding(top = 16.dp)
                    ) {
                        Box(
                            modifier = Modifier
                                .fillMaxSize()
                                .height(100.dp),
                            contentAlignment = Alignment.Center
                        ) {
                            Text(text = "Result")
                        }

                    }
                }
                item {
                    Box(
                        modifier = Modifier.fillMaxSize(),
                        contentAlignment = Alignment.Center
                    ) {
                        PieChart(
                            pieChartData = PieChartData(
                                slices = listOf(
                                    PieChartData.Slice(
                                        totalQuestion.toFloat() - correctNumber,
                                        Color.Red
                                    ),
                                    PieChartData.Slice(
                                        correctNumber.toFloat(),
                                        Color.Green
                                    ),
                                )
                            ),
                            // Optional properties.
                            modifier = Modifier
                                .size(200.dp)
                                .padding(top = 16.dp, bottom = 16.dp),
                            animation = simpleChartAnimation(),
                            sliceDrawer = SimpleSliceDrawer()
                        )


                        Text(
                            text = "$correctNumber/$totalQuestion",
                            textAlign = TextAlign.Center,
                            fontSize = 25.sp,
                            textDecoration = TextDecoration.None,
                            letterSpacing = 2.sp,
                            lineHeight = 32.sp,
                            overflow = TextOverflow.Ellipsis,
                            modifier = Modifier
                                .width(120.dp)
                                .alpha(1f),
                            fontWeight = FontWeight.Medium,
                            fontStyle = FontStyle.Normal,
                        )


                    }

                }
                item {
                    Box(
                        modifier = Modifier
                            .fillMaxSize()
                            .height(200.dp),
                        contentAlignment = Alignment.Center
                    ) {
                        if (calcInProgress) {
                            IndefiniteProgressIndicator()
                        } else if (calcFailed) {
                            Text( "Calculation failed,contact admin")
                        } else {
                            ScoreContent(
                                score = score,
                                bonusScore = bonusScore,
                                buff = bf,
                                rwd = rwd,
                                bonRwd = bonRwd
                            )
                        }
                    }
                }
            }

        }
    }
}

/**
 * TODO
 *
 * @param score
 * @param bonusScore bonusScore if bonus question attended
 * @param buff
 * @param rwd
 * @param bonRwd
 */
@Composable
private fun ScoreContent(
    score: Score,
    bonusScore: Score,
    buff: Buff,
    rwd: Reward,
    bonRwd: Reward
) {

    val padding = 16.dp

    Column(
        Modifier
            .padding(padding),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Row {// all rewards in row
            BadgeBox(
                badgeContent = {
                    Text(
                        text = String.format("%.2f", score.amount) ,
                        fontSize = 8.sp
                    )
                },
                modifier = Modifier
                    .padding(16.dp)
            ) {

                Box {
                    NetworkImage(
                        url = rwd.image_url,
                        contentDescription = rwd.name,
                        modifier = Modifier
                            .size(width = 40.dp, height = 40.dp)
                            .fillMaxSize(),
                        contentScale = ContentScale.Fit
                    )
                }
            }
            if (bonusScore.reward.isNotBlank()) {//when bonus score not generated reward will be blank
                BadgeBox(
                    badgeContent = {
                        Text(
                            text = String.format("%.2f", bonusScore.amount),
                            fontSize = 8.sp
                        )
                    },
                    modifier = Modifier
                        .padding(16.dp)
                ) {

                    Box {
                        NetworkImage(
                            url = bonRwd.image_url,
                            contentDescription = bonRwd.name,
                            modifier = Modifier
                                .size(width = 40.dp, height = 40.dp)
                                .fillMaxSize(),
                            contentScale = ContentScale.Fit
                        )
                    }
                }
            }

        }

        Box {
            NetworkImage(
                url = buff.image_url,
                contentDescription = buff.name,
                modifier = Modifier
                    .size(width = 40.dp, height = 40.dp)
                    .fillMaxSize(),
                contentScale = ContentScale.Fit
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun ResultsPreview() {

    val qlst = mutableListOf<QuestionState>()
    for (i in 1..10) {

        val q = Question(
            question = "Which country does this flag belong to",
            image = "",
            options = ArrayList<String>(listOf("Country1", "Country2", "Country3", "Country4")),
            correct_option = 0
        )
        val qState = QuestionState(
            id = "kkkkkkkkk",
            questionData = q,
            questionIndex = 0,
            totalQuestionsCount = 10,
            showPrevious = true,
            showDone = false

        )
        qState.answer = (0..3).random()
        qlst.add(qState)
    }

    val sc = Score(
        reward = "Toys",
        amount = 0.5,
        buff = "3_x_chance_toys"

    )

    val bonsc = Score(
        reward = "Food",
        amount = 10.0,

        )

    ResultContent(
        questionState = qlst,
        true,
        false,
        sc,
        bonsc,
        bf = Buff(
            name = "3_x_chance_toys",
            displayname = "3XCT",
            image_url = "https://firebasestorage.googleapis.com/v0/b/studentquiz-d86f5.appspot.com/o/buff-reward-images%2F2_X_amount_food.png?alt=media&token=b3ded7cc-f72a-4d3e-bef6-6a2e95bc827d"
        ),
        rwd = Reward(
            name = "Toys",
            image_url = "https://firebasestorage.googleapis.com/v0/b/studentquiz-d86f5.appspot.com/o/buff-reward-images%2Frwd_Study.png?alt=media&token=91042f6a-96e0-41a9-98fd-f71c2690457b"
        ),
        bonRwd = Reward(
            name = "Toys",
            image_url = "https://firebasestorage.googleapis.com/v0/b/studentquiz-d86f5.appspot.com/o/buff-reward-images%2Frwd_Study.png?alt=media&token=91042f6a-96e0-41a9-98fd-f71c2690457b"
        )
    )
}

@Preview(showBackground = true)
@Composable
fun ScorePreview() {
    val sc = Score(
        reward = "Toys",
        amount = 0.5,
        buff = "3_x_chance_toys"

    )

    val bonsc = Score(
        reward = "Food",
        amount = 10.0,

        )
    ScoreContent(
        score = sc,
        bonsc,
        buff = Buff(
            name = "3_x_chance_toys",
            displayname = "3XCT",
            image_url = "https://firebasestorage.googleapis.com/v0/b/studentquiz-d86f5.appspot.com/o/buff-reward-images%2F2_X_amount_food.png?alt=media&token=b3ded7cc-f72a-4d3e-bef6-6a2e95bc827d"
        ),
        rwd = Reward(
            name = "Toys",
            image_url = "https://firebasestorage.googleapis.com/v0/b/studentquiz-d86f5.appspot.com/o/buff-reward-images%2Frwd_Study.png?alt=media&token=91042f6a-96e0-41a9-98fd-f71c2690457b"
        ),
        bonRwd = Reward(
            name = "Toys",
            image_url = "https://firebasestorage.googleapis.com/v0/b/studentquiz-d86f5.appspot.com/o/buff-reward-images%2Frwd_Study.png?alt=media&token=91042f6a-96e0-41a9-98fd-f71c2690457b"
        )
    )
}