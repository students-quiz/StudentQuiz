package com.app.studentquiz.screens.students

import androidx.compose.runtime.Stable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import com.app.studentquiz.repo.users.User
import com.app.studentquiz.screens.login.LoginRepository
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.toObject
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class StudentScreenViewModel @Inject constructor(
    val db: FirebaseFirestore,
    private val authRepo: LoginRepository,
) : ViewModel() {
    val uiState = UIState()

    /**
     * Load all student where teacher = current user uid
     *
     */
    fun loadStudents() {
        uiState.studentListLoading = true
        val firebaseUser = authRepo.currentUser()
        firebaseUser?.let { fu ->
            db.collection("users")
                .whereEqualTo("teacher", fu.uid)
                .get()
                .addOnSuccessListener { qsLst ->

                    val studentList = mutableListOf<User>()
                    qsLst.forEach { qs ->
                        val u = qs.toObject<User>()
                        u.uid = qs.id


                        studentList.add(u)


                    }
                    // set ui states
                    uiState.studentList = studentList
                    uiState.studentListLoading = false
                }
        }
    }
}

/**
 * all state variables
 * @ Stable make sure updating all public variables
 * forces recomposition where used
 */
@Stable
class UIState {
    var studentListLoading by mutableStateOf(false)
    var studentList by mutableStateOf<List<User>>(mutableListOf())
}