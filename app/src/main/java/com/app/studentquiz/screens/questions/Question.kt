package com.app.studentquiz.screens.questions

data class Question(

    var question: String = "",
    var solution: String = "",
    var image: String = "",
    var hint: String = "",
    var yt_video_id: String = "",
    var correct_option: Int = 0,
    var didyouknow: String = "",
    var options: List<String> = ArrayList(),
    var tags: ArrayList<String> = ArrayList(),
    var bonusParent: String = ""
)
