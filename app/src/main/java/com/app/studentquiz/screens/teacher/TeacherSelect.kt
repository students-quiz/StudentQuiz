package com.app.studentquiz.screens.teacher

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.app.studentquiz.screens.profile.Teacher

@Composable
fun TeacherSelectScreen(
    vm: TeacherSelectViewModel = hiltViewModel(),
    navController: NavController,
    curTeacher: String?
) {
    LaunchedEffect(key1 = true) {
        // current teacher
        curTeacher?.let {
            vm.uiState.curTeacher = Teacher(
                name = curTeacher
            )
        }
        vm.loadTeacherList()
    }
    TeacherListContent(
        vm.uiState.teacherListLoading,
        vm.uiState.teacherList,
        vm.uiState.curTeacher
    ) { tc -> vm.selectTeacher(tc) }
}

@Composable
private fun TeacherListContent(
    teacherListLoading: Boolean,
    teacherList: List<Teacher>,
    curTeacher: Teacher,
    onTeacherSelect: (Teacher) -> Unit
) {
    LazyColumn {
        item {
            Surface(
                modifier = Modifier.padding(4.dp),
                elevation = 1.dp,
            ) {
                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(16.dp)
                        .background(
                            MaterialTheme.colors.secondary,
                            MaterialTheme.shapes.medium
                        ),
                    contentAlignment = Alignment.Center,
                ) {
                    Row {
                        Text(text = "Teacher: ")
                        Text(text = curTeacher.name)

                    }
                }
            }


        }

        if (!teacherListLoading) {
            // list of users with role teacher = true
            teacherList.forEach { tc ->
                item {
                    Surface(
                        modifier = Modifier
                            .padding(4.dp),
                        elevation = 1.dp,
                    ) {
                        Box(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(16.dp)
                                .background(
                                    MaterialTheme.colors.secondaryVariant,
                                    MaterialTheme.shapes.small
                                )
                                .clickable {
                                    onTeacherSelect(tc)
                                },
                            contentAlignment = Alignment.Center
                        )
                        {
                            Column {
                                Text(text = tc.name)
                                Text(text = tc.email)
                            }
                        }
                    }

                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun TeacherListPreview() {
    val curTeacher = Teacher(
        name = "Babai Wizard"
    )

    val teacherList = listOf(
        Teacher(
            name = "Babai Wizard"
        ),
        Teacher(
            name = "Arnab Senapati"
        ),
        Teacher(
            name = "Taniya Das"
        ),
        Teacher(
            name = "Argha Das"
        ),
        Teacher(
            name = "Babai Wizard"
        ),
        Teacher(
            name = "Babai Wizard"
        )
    )

    TeacherListContent(
        teacherListLoading = false,
        teacherList = teacherList,
        curTeacher = curTeacher,
        onTeacherSelect = { }
    )
}