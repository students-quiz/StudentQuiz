package com.app.studentquiz.screens.teacher

import androidx.compose.runtime.Stable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import com.app.studentquiz.repo.users.User
import com.app.studentquiz.screens.login.LoginRepository
import com.app.studentquiz.screens.profile.Teacher
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions
import com.google.firebase.firestore.ktx.toObject
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class TeacherSelectViewModel @Inject constructor(
    val db: FirebaseFirestore,
    private val authRepo: LoginRepository,
) : ViewModel() {
    val uiState = UIState()

    /**
     *
     * list of users with role teacher = true
     */
    fun loadTeacherList() {
        uiState.teacherListLoading = true
        val firebaseUser = authRepo.currentUser()
        firebaseUser?.let { fu->
            db.collection("users")
                .whereEqualTo("roles.teacher", true)
                .get()
                .addOnSuccessListener { qsLst ->

                    val teacherList = mutableListOf<Teacher>()
                    qsLst.forEach { qs ->
                        if(qs.id != fu.uid){// do not select yourself as teacher
                            val u = qs.toObject<User>()
                            val teacher = Teacher(
                                uid = qs.id,
                                name = u.name,
                                email = u.email
                            )
                            teacherList.add(teacher)
                        }

                    }
                    // set ui states
                    uiState.teacherList = teacherList
                    uiState.teacherListLoading = false
                }
        }

    }

    /**
     * Select teacher
     *
     * @param tc
     */
    fun selectTeacher(tc : Teacher){
        val firebaseUser = authRepo.currentUser()
        firebaseUser?.let {
            val data = hashMapOf("teacher" to tc.uid)
            db.collection("users")
                .document(firebaseUser.uid)
                .set(data, SetOptions.merge()) // set only teacher field and merge
                .addOnSuccessListener {
                    uiState.teacherSelected = true
                    uiState.curTeacher = tc
                }
        }
    }
}

/**
 * all state variables
 * @Stable make sure updating all public variables
 * forces recomposition where used
 */
@Stable
class UIState {
    var teacherListLoading by mutableStateOf(false)
    var teacherSelected by mutableStateOf(false)
    var teacherList by mutableStateOf<List<Teacher>>(mutableListOf())
    var curTeacher by mutableStateOf(Teacher())
}