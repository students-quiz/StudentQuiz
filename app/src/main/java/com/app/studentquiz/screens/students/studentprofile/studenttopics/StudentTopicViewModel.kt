package com.app.studentquiz.screens.students.studentprofile.studenttopics

import androidx.compose.runtime.Stable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import com.app.studentquiz.repo.users.User
import com.app.studentquiz.screens.categoryselection.Category
import com.app.studentquiz.screens.categoryselection.CategoryRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class StudentTopicViewModel @Inject constructor(
    private val catRepo: CategoryRepository
) : ViewModel() {

    val uiState = UIState()
    fun loadStudentTopics(student: User?){
        student?.let { st ->
            catRepo.getLeafTags(st.uid){ catList ->
                uiState.leafTags = catList
            }
        }
    }
}


/**
 * all state variables
 * @ Stable make sure updating all public variables
 * forces recomposition where used
 */
@Stable
class UIState {
    var leafTags by mutableStateOf(listOf<Category>())
}