@file:OptIn(ExperimentalMaterialApi::class)

package com.app.studentquiz.screens.profile

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowForwardIos
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.app.studentquiz.common.component.NetworkImage
import com.app.studentquiz.common.objectclasses.Buff
import com.app.studentquiz.common.objectclasses.Reward
import com.app.studentquiz.repo.users.ActiveBuff
import com.app.studentquiz.repo.users.User
import com.app.studentquiz.ui.theme.blue200
import com.app.studentquiz.ui.theme.blue800
import com.google.accompanist.flowlayout.FlowRow
import java.util.*

@Composable
fun ProfileScreenContent(
    signOutOnClick: () -> Unit,
    userProfile: User,
    teacher: Teacher,
    showTeacher: Boolean,
    selectTeacherClick: () -> Unit,
    showUserProfile: Boolean,
    showBuffs: Boolean,
    activateBuffClick: (buff: String) -> Unit,
    buffs: List<Buff>,
    rewards: List<Reward>,
    globalBuff: ActiveBuff,
    showRewards: Boolean,
    onRewardsClick: (Reward) -> Unit // only for teachers rewards click event

) {
    LazyColumn(
        contentPadding = PaddingValues(start = 20.dp, end = 20.dp)
    ) {

        if (showUserProfile) {
            item {
                UserInfoContent(userProfile)
            }
        }
        if (showTeacher) {
            item { SeparatorContent() }
            item {
                TeacherContent(
                    teacher = teacher,
                    selectTeacherClick = selectTeacherClick
                )
            }
        }
        if (showBuffs) {

            item {
                SeparatorContent()
            }

            item {
                ActiveBuffContent(userProfile.activeBuff, buffs, globalBuff)
            }

            item {
                SeparatorContent()
            }
            item {
                BuffCollectionContent(userProfile, buffs, activateBuffClick)
            }
        }
        if (showRewards) {

            item {
                SeparatorContent()
            }

            item {
                RewardsContent(
                    userProfile,
                    rewards,
                    onRewardsClick
                )
            }
        }

        item {
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(vertical = 16.dp),
                contentAlignment = Alignment.Center,
            ) {
                Button(
                    onClick = signOutOnClick,
                ) {
                    Text(text = "SignOut")
                }
            }
        }
    }

}

/**
 * Show current teacher and allows to select
 *
 * @param teacher
 * @param selectTeacherClick , select teacher
 */
@Composable
private fun TeacherContent(
    teacher: Teacher,
    selectTeacherClick: () -> Unit
) {
    val isTeacherPresent = teacher.name.isNotBlank()
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxWidth()
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(text = "Teacher: ")
            if (isTeacherPresent) {
                Text(text = teacher.name)
            } else {
                Text(text = "Choose your teacher ")
            }
            IconButton(
                modifier = Modifier
                    .size(width = 50.dp, height = 24.dp)
                    .padding(start = 10.dp),
                onClick = selectTeacherClick
            ) {
                Icon(
                    imageVector = Icons.Filled.ArrowForwardIos,
                    contentDescription = "Choose Teacher"
                )
            }
        }
    }
}

@Composable
private fun RewardsContent(
    userProfile: User,
    rewards: List<Reward>,
    onRewardsClick: (Reward) -> Unit
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxWidth()
    ) {
        Text(text = "Rewards Collection")
        FlowRow {
            userProfile.rewards.forEach {
                val r = rewards.first { rwd -> rwd.name == it.key }
                BadgeBox(
                    badgeContent = {
                        Text(
                            text = String.format("%.2f", it.value) ,
                            fontSize = 8.sp
                        )
                    },
                    modifier = Modifier
                        .padding(16.dp)
                ) {

                    Box(
                        modifier = Modifier.clickable(
                            // take action on the reward got clicked
                            onClick = { onRewardsClick(r) },
                        ),
                    ) {
                        NetworkImage(
                            url = r.image_url,
                            contentDescription = r.name,
                            modifier = Modifier
                                .size(width = 40.dp, height = 40.dp)
                                .fillMaxSize(),
                            contentScale = ContentScale.Fit
                        )
                    }
                }
            }
        }
    }
}

@Composable
private fun BuffCollectionContent(
    userProfile: User,
    buffs: List<Buff>,
    activateBuffClick: (buff: String) -> Unit
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxWidth()
    ) {

        Text(text = "Buff Collection")
        FlowRow {
            userProfile.buffs.forEach {
                val b = buffs.first { bf -> bf.name == it.key }
                BadgeBox(
                    badgeContent = {
                        Text(
                            text = it.value.toString(),
                            fontSize = 8.sp
                        )
                    },
                    modifier = Modifier
                        .padding(16.dp)
                ) {

                    Box(
                        modifier = Modifier.clickable(
                            onClick = { activateBuffClick(it.key) },
                        ),
                    ) {
                        NetworkImage(
                            url = b.image_url,
                            contentDescription = b.name,
                            modifier = Modifier
                                .size(width = 40.dp, height = 40.dp)
                                .fillMaxSize(),
                            contentScale = ContentScale.Fit
                        )
                    }
                }
            }

        }


    }
}


@Composable
private fun ActiveBuffContent(
    activeBuff: ActiveBuff?,
    buffs: List<Buff>,
    globalBuff: ActiveBuff
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxWidth()
    ) {

        Text(text = "Active Buff")
        FlowRow {
            activeBuff?.let {
                val timeLeft = getTimeLeft(activeBuff.expireAt)
                if (timeLeft > 0) {
                    val b = buffs.first { bf -> bf.name == activeBuff.buff }
                    BadgeBox(
                        badgeContent = {
                            Text(
                                text = "${timeLeft}m",
                                fontSize = 8.sp
                            )
                        },
                        modifier = Modifier
                            .padding(16.dp),
                        backgroundColor = blue800,
                        contentColor = blue200
                    ) {

                        Box {
                            NetworkImage(
                                url = b.image_url,
                                contentDescription = b.name,
                                modifier = Modifier
                                    .size(width = 40.dp, height = 40.dp)
                                    .fillMaxSize(),
                                contentScale = ContentScale.Fit
                            )
                        }
                    }
                }
            }
            if (globalBuff.buff.isNotEmpty()) {
                val timeLeft = getTimeLeft(globalBuff.expireAt)
                if (timeLeft > 0) {
                    val b = buffs.first { bf -> bf.name == globalBuff.buff }
                    BadgeBox(
                        badgeContent = {
                            Text(
                                text = "${timeLeft}m",
                                fontSize = 8.sp
                            )
                        },
                        modifier = Modifier
                            .padding(16.dp),
                        backgroundColor = blue800,
                        contentColor = blue200
                    ) {

                        Box {
                            NetworkImage(
                                url = b.image_url,
                                contentDescription = b.name,
                                modifier = Modifier
                                    .size(width = 40.dp, height = 40.dp)
                                    .fillMaxSize(),
                                contentScale = ContentScale.Fit
                            )
                        }
                    }
                }
            }
        }


    }
}

@Composable
private fun SeparatorContent() {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxWidth()
    ) {

        Divider(
            modifier = Modifier.padding(vertical = 16.dp)
        )
    }
}

@Composable
private fun UserInfoContent(userProfile: User) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxWidth()
    ) {
        Text(text = userProfile.name)
        Text(text = userProfile.email)

    }
}

private fun getTimeLeft(time: Long): Long {

    val currTime = Date().time
    var diff: Long = 0
    if (time > currTime) {
        diff = (time - currTime) / 1000 / 60
    }
    return diff

}