package com.app.studentquiz.screens.categoryselection

import com.app.studentquiz.repo.users.User
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.firestore.FirebaseFirestore
import javax.inject.Inject

const val VIDEO_CAT = "Video"
class CategoryRepository @Inject constructor(

    val db: FirebaseFirestore,
)
{
    /**
     * gets the list of categories with the number of questions and
     * number of questions userId already attended
     *
     *
     * @param userId user for which number of questions per tag will be selected
     * @param onSuccess what to do with the List of Category
     */
    fun getLeafTags(
        userId: String,
        onSuccess: (List<Category>) -> Unit
    ){

        db.collection("users")
            .document(userId)
            .get()
            .addOnSuccessListener { doc ->
                val user = doc.toObject(User::class.java)!!
                db.collection(TAGS)
                    .whereEqualTo("leaf", true)
                    .get()
                    .addOnSuccessListener {
                        // Here we have created a new instance for Question ArrayList.
                        val catList: ArrayList<Category> = ArrayList()
                        // by default Video category is added, its a special category
                        // includes all questions that has video
                        catList.add(
                            Category(
                            display_name = VIDEO_CAT,
                            leaf = true
                        )
                        )
                        // A for loop as per the list of documents to convert them into Boards ArrayList.
                        for (i in it.documents) {
                            val cat = i.toObject(Category::class.java)!!
                            user.tagscount[cat.display_name]?.let { cnt ->
                                cat.userCount = cnt.toInt()
                            }
                            catList.add(cat)

                        }
                        onSuccess(catList)
                    }
            }

    }
}