package com.app.studentquiz.screens.profile

import androidx.compose.runtime.Stable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import com.app.studentquiz.common.objectclasses.Buff
import com.app.studentquiz.common.objectclasses.Reward
import com.app.studentquiz.repo.users.ActiveBuff
import com.app.studentquiz.repo.users.User

/**
 * keeps all the variables for profile state
 * @ stable so that change in the variables makes recomposition
 */
@Stable
class ProfileUIState {
    var userFetchInProgress by mutableStateOf(false)
    var buffFetchInProgress by mutableStateOf(false)
    var rewardsFetchInProgress by mutableStateOf(false)
    var teacherFetchInProgress by mutableStateOf(false)
    var userProfile by mutableStateOf(User())
    var teacherProfile by mutableStateOf(Teacher())
    var buffs by mutableStateOf<List<Buff>>(mutableListOf())
    var rewards by mutableStateOf<List<Reward>>(mutableListOf())
    var gBuff by mutableStateOf(ActiveBuff())
}

data class Teacher(
    val uid: String = "",
    val name: String = "",
    val profilePicUrl: String = "",
    val email: String = ""
)