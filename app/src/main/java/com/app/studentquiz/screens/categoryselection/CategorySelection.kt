/*
 * Copyright 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.app.studentquiz.screens.categoryselection

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material.icons.rounded.Explore
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.app.studentquiz.R
import com.app.studentquiz.screens.StudentQuizRoutes
import com.app.studentquiz.ui.theme.StudentQuizAndroidTheme

const val SELECTED_CATEGORIES = "selectedCategories"

@Composable
fun CatSelection(vm: CategoryViewModel, navController: NavHostController) {
    StudentQuizAndroidTheme() {

        LaunchedEffect(key1 = Unit) {
            vm.selectedTags.value = listOf<String>()
            vm.getLeafTags()
        }

        Scaffold(
//            topBar = { AppBar() },
            backgroundColor = MaterialTheme.colors.primarySurface,
            floatingActionButton = {
                FloatingActionButton(
                    onClick = {

                        navController.currentBackStackEntry?.savedStateHandle?.set(
                            SELECTED_CATEGORIES,
                            vm.selectedTags.value
                        )
                        navController.navigate(StudentQuizRoutes.Questions.route)
                    },
                    modifier = Modifier.padding(bottom = 50.dp)
                ) {
                    Icon(
                        imageVector = Icons.Rounded.Explore,
                        contentDescription = "Continue to course"
                    )
                }
            }
        ) { innerPadding ->

            Image(
                painter = painterResource(id = R.drawable.signinbg),
                contentDescription = "Background",
                Modifier.fillMaxWidth(),
                contentScale = ContentScale.FillBounds
            )
            Column(
                modifier = Modifier
                    .statusBarsPadding()
                    .navigationBarsPadding()
                    .padding(innerPadding)
            ) {
                Text(
                    text = "Choose Topics That Interests You",
                    style = MaterialTheme.typography.h4,
                    textAlign = TextAlign.End,
                    modifier = Modifier.padding(
                        horizontal = 16.dp,
                        vertical = 32.dp
                    )
                )
                TopicsGrid(
                    modifier = Modifier
                        .weight(1f)
                        .wrapContentHeight(),
                    leafTags = vm.leafTags.value,
                    onSelectTag = vm::selectTag
                )
                Spacer(Modifier.height(56.dp)) // center grid accounting for FAB
            }
        }
    }
}


@Composable
private fun AppBar() {
    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .fillMaxWidth()
            .statusBarsPadding()
    ) {
        Image(
            painter = painterResource(id = R.drawable.ic_baseline_login_24),
            contentDescription = null,
            modifier = Modifier.padding(16.dp)
        )
        IconButton(
            modifier = Modifier.padding(16.dp),
            onClick = { /* todo */ }
        ) {
            Icon(
                imageVector = Icons.Filled.Settings,
                contentDescription = "Settings"
            )
        }
    }
}



@Preview(showBackground = true)
@Composable
fun TopicsGridPreview() {
    val catList = listOf<Category>(
        Category(
            display_name = "Physics",
            count = (0..10).random(),
            userCount = (0..10).random()
        ),
        Category(
            display_name = "Science",
            count = (0..10).random(),
            userCount = (0..10).random()
        ),
        Category(
            display_name = "Chemistry",
            count = (0..10).random(),
            userCount = (0..10).random()
        ),
        Category(
            display_name = "Astronomy",
            count = (0..10).random(),
            userCount = (0..10).random()
        )
    )

    TopicsGrid(
        leafTags = catList,
        onSelectTag = {}
    )
}


