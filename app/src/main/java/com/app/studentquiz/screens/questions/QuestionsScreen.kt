package com.app.studentquiz.screens.questions

import android.content.pm.ActivityInfo
import android.widget.Toast
import androidx.compose.animation.animateColor
import androidx.compose.animation.core.*
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.selection.selectable
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.app.studentquiz.R
import com.app.studentquiz.common.component.Html
import com.app.studentquiz.common.component.LockScreenOrientation
import com.app.studentquiz.common.component.NetworkImage
import com.app.studentquiz.common.component.Youtube
import com.app.studentquiz.screens.StudentQuizRoutes
import com.app.studentquiz.ui.theme.*

const val QUESTION_ANSWER = "questionAnswer"

@Composable
fun QuestionsScreen(
    vm: QuestionViewModel,
    navController: NavController,
    selectedCategories: List<String>?
) {
    LaunchedEffect(key1 = selectedCategories) {
        vm.questionsLoaded.value = false
        vm.loadQuestions(selectedCategories = selectedCategories)
    }
    //Fix orientation PORTRAIT
    LockScreenOrientation(orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
    val showDidYouKow = remember { mutableStateOf(false) }
    if (vm.questionsLoaded.value) {
        if (vm.showingOldQuestions.value) {
            // show message for first question only
            Toast.makeText(
                LocalContext.current,
                "No New Question, Showing old questions",
                Toast.LENGTH_LONG
            ).show()
        }
        val questionState = vm.questions.value[vm.currentQuestionIndex.value]

        //color animation between two colors
        val infiniteTransition = rememberInfiniteTransition()
        val color by infiniteTransition.animateColor(
            initialValue = yellow200,
            targetValue = yellow400,
            animationSpec = infiniteRepeatable(
                animation = tween(1000, easing = LinearEasing),
                repeatMode = RepeatMode.Restart
            )
        )

        Surface {
            Scaffold(
                topBar = {
                    QuestionsTopAppBar(
                        questionIndex = questionState.questionIndex,
                        totalQuestionsCount = questionState.totalQuestionsCount,
                        //on pressing close button return to Home (Category selection)
                        onBackPressed = { navController.navigate(StudentQuizRoutes.Home.route) },
                        // different color for bonus question
                        bgColor = if (questionState.questionData.bonusParent.isBlank()) Teal200 else color
                    )
                },
                content = { innerPadding ->
                    if (showDidYouKow.value) {
                        LazyColumn {//for vertical scrolling of the content
                            item {
                                Box(
                                    modifier = Modifier.padding(
                                        top = 24.dp,
                                        bottom = 100.dp,
                                        start = 24.dp,
                                        end = 24.dp
                                    )
                                ) {
                                    Html(text = questionState.questionData.didyouknow)
                                }
                            }

                        }
                    } else {
                        QuestionContent(
                            questionState,
                            onAnswer = onAnswerQ(questionState),
                            submitted = questionState.submitted,
                            optionEnabled = !questionState.submitted,
                            onDidUKnowClick = {
                                showDidYouKow.value = true
                                if (!questionState.bonusAdded) {
                                    // only once bonus questions can be added for a bonusParent
                                    vm.addBonusQs(questionState.id)
                                    questionState.bonusAdded = true
                                }
                            },
                            modifier = Modifier
                                .fillMaxSize()
                                .padding(innerPadding)
                        )
                    }

                },
                bottomBar = {
                    QuestionBottomBar(
                        questionState = questionState,
                        onPreviousPressed = { vm.currentQuestionIndex.value-- },
                        onNextPressed = {
                            showDidYouKow.value = false
                            // show old question message only for the first question
                            vm.showingOldQuestions.value = false
                            if (questionState.submitted) {
                                if (questionState.questionData.yt_video_id.isNotBlank()
                                    && !questionState.bonusAdded
                                ) {
                                    // if a video question then add bonus question on submit
                                    vm.addBonusQs(questionState.id)
                                    questionState.bonusAdded = true
                                }
                                vm.currentQuestionIndex.value++
                            } else {
                                questionState.submitted = true
                            }
                        },
                        onDonePressed = onDonePressed(
                            navController = navController,
                            vm = vm,
                            questionState = questionState
                        )
                    )
                }
            )
        }
    }


}

@Composable
private fun onDonePressed(
    vm: QuestionViewModel,
    navController: NavController,
    questionState: QuestionState
): () -> Unit =
    {
        // if last question is a video question then add bonus for that
        // other wise navigate to the result
        if (questionState.questionData.yt_video_id.isNotBlank()
            && !questionState.bonusAdded
        ) {
            vm.addBonusQs(questionState.id)
            questionState.bonusAdded = true
            //if bonus is added move to next question
            vm.currentQuestionIndex.value++
        }else{
            navController.currentBackStackEntry?.savedStateHandle?.set(
                QUESTION_ANSWER,
                vm.questions.value
            )
            navController.navigate(StudentQuizRoutes.Results.route)
        }

    }

@Composable
private fun onAnswerQ(questionState: QuestionState): (Int) -> Unit =
    {
        questionState.answer = it
        questionState.enableNext = true
    }

@Composable
private fun QuestionContent(
    questionState: QuestionState,
    modifier: Modifier,
    submitted: Boolean,
    optionEnabled: Boolean,
    onAnswer: (Int) -> Unit,
    onDidUKnowClick: (Int) -> Unit,
) {
    LazyColumn(
        modifier = modifier,
        contentPadding = PaddingValues(start = 20.dp, end = 20.dp)
    ) {
        item {
            Spacer(modifier = Modifier.height(32.dp))
            QuestionTitle(questionState.questionData.question)
            Spacer(modifier = Modifier.height(16.dp))
            Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ) {
                if (questionState.questionData.yt_video_id.isNotBlank()) {
                    // if youtube video is there don't show image
                    Youtube(videoId = questionState.questionData.yt_video_id)
                } else {
                    NetworkImage(
                        url = questionState.questionData.image,
                        contentDescription = questionState.questionData.question,
                        modifier = Modifier
                            .size(width = 250.dp, height = 100.dp)
                            .aspectRatio(1.5f)
                            .padding(top = 16.dp, bottom = 16.dp)
                            .fillMaxSize(),
                        contentScale = ContentScale.Fit
                    )
                }

            }


            Spacer(modifier = Modifier.height(16.dp))
            SingleChoiceQuestion(
                options = questionState.questionData.options,
                onAnswerSelected = onAnswer,
                correctOption = questionState.questionData.correct_option,
                submitted = submitted,
                optionEnabled = optionEnabled,
                modifier = Modifier.fillParentMaxWidth()
            )
            //If did you know is too small assume there is no did you know
            // for blank did you know "<p></p>" is saved, can be assumed blank
            // Show did you know only when submitted
            if (questionState.submitted
                && questionState.questionData.didyouknow.length > 10) ClickableText(
                text = buildAnnotatedString {
                    // underlined text with theme color on Background
                    withStyle(
                        style = SpanStyle(
                            color = MaterialTheme.colors.onBackground,
                            textDecoration = TextDecoration.Underline
                        )
                    ) {
                        append("Did You Know....")

                    }
                },
                onClick = onDidUKnowClick,

                )

        }
    }
}


@Composable
private fun SingleChoiceQuestion(
    options: List<String>,
    onAnswerSelected: (Int) -> Unit,
    correctOption: Int,
    submitted: Boolean,
    optionEnabled: Boolean,
    modifier: Modifier = Modifier
) {


    val (selectedOption, onOptionSelected) = remember { mutableStateOf("") }

    Column(modifier = modifier) {
        options.forEachIndexed { index, text ->
            val onClickHandle = {
                if (!submitted) {
                    // answer can not be modified after submitted
                    onOptionSelected(text)
                    onAnswerSelected(index)
                }
            }
            val optionSelected = text == selectedOption
            val correctOptionThis = index == correctOption
            var showResult = false
            val resBgColor =
                if (submitted && correctOptionThis) {
                    showResult = true
                    MaterialTheme.colors.secondary.copy(alpha = 0.5f)

                } else if (optionSelected && submitted && !correctOptionThis) {
                    showResult = true
                    MaterialTheme.colors.error.copy(alpha = 0.3f)

                } else if (optionSelected) {
                    MaterialTheme.colors.primary.copy(alpha = 0.5f)
                } else {
                    MaterialTheme.colors.background
                }


            val answerBorderColor =
                if (optionSelected && !submitted) {
                    MaterialTheme.colors.primary.copy(alpha = 0.8f)
                } else {
                    MaterialTheme.colors.onSurface.copy(alpha = 0.12f)
                }
            val answerBackgroundColor = resBgColor
            Surface(
                shape = MaterialTheme.shapes.small,
                border = BorderStroke(
                    width = 1.dp,
                    color = answerBorderColor
                ),
                modifier = Modifier.padding(vertical = 8.dp)
            ) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .selectable(
                            selected = optionSelected,
                            onClick = onClickHandle
                        )
                        .background(answerBackgroundColor)
                        .padding(vertical = 16.dp, horizontal = 16.dp),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(
                        text = text
                    )


                }
            }
        }
    }
}


@Composable
private fun QuestionTitle(title: String) {
    val backgroundColor = if (MaterialTheme.colors.isLight) {
        MaterialTheme.colors.onSurface.copy(alpha = 0.04f)
    } else {
        MaterialTheme.colors.onSurface.copy(alpha = 0.06f)
    }
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .background(
                color = backgroundColor,
                shape = MaterialTheme.shapes.small
            )
    ) {
        Text(
            text = title,
            style = MaterialTheme.typography.subtitle1,
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 24.dp, horizontal = 16.dp)
        )
    }
}


@Composable
private fun QuestionsTopAppBar(
    questionIndex: Int,
    totalQuestionsCount: Int,
    onBackPressed: () -> Unit,
    bgColor: Color
) {
    Column(modifier = Modifier.fillMaxWidth()) {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp)
                .background(
                    bgColor,
                    shape = RoundedCornerShape(4.dp)
                )
        ) {
            TopAppBarTitle(
                questionIndex = questionIndex,
                totalQuestionsCount = totalQuestionsCount,
                modifier = Modifier
                    .padding(vertical = 20.dp)
                    .align(Alignment.Center)
            )

            CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.medium) {
                IconButton(
                    onClick = onBackPressed,
                    modifier = Modifier
                        .padding(horizontal = 20.dp, vertical = 20.dp)
                        .fillMaxWidth()
                ) {
                    Icon(
                        Icons.Filled.Close,
                        contentDescription = stringResource(id = R.string.close),
                        modifier = Modifier.align(Alignment.CenterEnd)
                    )
                }
            }
        }
        val animatedProgress by animateFloatAsState(
            targetValue = (questionIndex + 1) / totalQuestionsCount.toFloat(),
            animationSpec = ProgressIndicatorDefaults.ProgressAnimationSpec
        )
        LinearProgressIndicator(
            progress = animatedProgress,
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 20.dp),
            backgroundColor = MaterialTheme.colors.progressIndicatorBackground
        )
    }
}

@Composable
private fun QuestionBottomBar(
    questionState: QuestionState,
    onPreviousPressed: () -> Unit,
    onNextPressed: () -> Unit,
    onDonePressed: () -> Unit
) {
    Surface(
        elevation = 7.dp,
        modifier = Modifier.fillMaxWidth() // .border(1.dp, MaterialTheme.colors.primary)
    ) {

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp, vertical = 20.dp)
        ) {
            if (questionState.showPrevious) {
                OutlinedButton(
                    modifier = Modifier
                        .weight(1f)
                        .height(48.dp),
                    onClick = onPreviousPressed
                ) {
                    Text(text = stringResource(id = R.string.previous))
                }
                Spacer(modifier = Modifier.width(16.dp))
            }
            if (questionState.showDone && questionState.submitted) {
                Button(
                    modifier = Modifier
                        .weight(1f)
                        .height(48.dp),
                    onClick = onDonePressed,
                    enabled = questionState.enableNext
                ) {
                    Text(text = stringResource(id = R.string.done))
                }
            } else {
                Button(
                    modifier = Modifier
                        .weight(1f)
                        .height(48.dp),
                    onClick = onNextPressed,
                    enabled = questionState.enableNext
                ) {
                    if (questionState.submitted) {
                        Text(text = stringResource(id = R.string.next))
                    } else {
                        Text(text = stringResource(id = R.string.submit))
                    }
                }
            }
        }
    }
}


@Composable
private fun TopAppBarTitle(
    questionIndex: Int,
    totalQuestionsCount: Int,
    modifier: Modifier = Modifier
) {
    val indexStyle = MaterialTheme.typography.caption.toSpanStyle().copy(
        fontWeight = FontWeight.Bold
    )
    val totalStyle = MaterialTheme.typography.caption.toSpanStyle()
    val questionCount = stringResource(R.string.question_count, totalQuestionsCount)
    val text = buildAnnotatedString {
        withStyle(style = indexStyle) {
            append("${questionIndex + 1}")
        }
        withStyle(style = totalStyle) {
            append(questionCount)
        }
    }
    Text(
        text = text,
        style = MaterialTheme.typography.caption,
        modifier = modifier
    )
}

@Preview
@Composable
fun QuestionScreenPreview() {
    val q = Question(
        question = "Which country does this flag belong to",
        image = "",
        options = ArrayList<String>(listOf("Country1", "Country2", "Country3", "Country4")),
        correct_option = 0
    )
    val qState = QuestionState(
        id = "aksjddhskfjfh",
        questionData = q,
        questionIndex = 0,
        totalQuestionsCount = 10,
        showPrevious = true,
        showDone = false

    )

    StudentQuizAndroidTheme {
        QuestionContent(
            questionState = qState,
            onAnswer = { },
            submitted = true,
            optionEnabled = true,
            onDidUKnowClick = {},
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp)
        )
    }
}