package com.app.studentquiz.screens.students

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Button
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.app.studentquiz.repo.users.User
import com.app.studentquiz.screens.StudentQuizRoutes
import com.app.studentquiz.screens.students.studentprofile.STUDENT_PROFILE
import com.app.studentquiz.screens.students.studentprofile.studenttopics.STUDENT_TOPIC

@Composable
fun StudentsScreen(
    vm: StudentScreenViewModel = hiltViewModel(),
    navController: NavController
) {

    LaunchedEffect(key1 = true) {
        vm.loadStudents()
    }

    if (!vm.uiState.studentListLoading) {
        StudentListContent(vm, navController)
    }


}

@Composable
private fun StudentListContent(
    vm: StudentScreenViewModel,
    navController: NavController
) {
    LazyColumn {
        vm.uiState.studentList.forEach { st ->
            item {
                StudentItem(st, navController)
            }
        }

    }
}

@Composable
private fun StudentItem(
    st: User,
    navController: NavController
) {
    Surface(
        modifier = Modifier.padding(4.dp),
        elevation = 1.dp,
    ) {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp),
            contentAlignment = Alignment.Center,
        ) {
            Column {
                Text(text = st.name)
                Text(text = st.email)
                Row {
                    StudentButton(
                        key = STUDENT_PROFILE,
                        route = StudentQuizRoutes.StudentsProfile.route,
                        text = "Profile",
                        st,
                        navController
                    )
                    StudentButton(
                        key = STUDENT_TOPIC,
                        route = StudentQuizRoutes.StudentsTopics.route,
                        text = "Topics",
                        st,
                        navController
                    )
                }
            }
        }
    }
}

@Composable
fun StudentButton(
    key: String,
    route: String,
    text: String,
    st: User,
    navController: NavController
) {
    Button(
        onClick = {
            //send parcelize student to student profile screen
            navController.currentBackStackEntry?.savedStateHandle?.set(
                key,
                st
            )
            navController.navigate(route)
        },
        modifier = Modifier.padding(16.dp)
    ) {
        Text(text = text)
    }
}

