package com.app.studentquiz.screens

import android.view.animation.OvershootInterpolator
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.app.studentquiz.R
import kotlinx.coroutines.delay

@Composable
fun QuizSplashScreen(navController: NavController) {

    Surface() {
        Image(
            painter = painterResource(id = R.drawable.splash_screen_bg),
            contentDescription = "Background",
            Modifier.fillMaxWidth(),
            contentScale = ContentScale.FillBounds
        )
        Logo(navController = navController)
    }


}

@Composable
fun Logo(navController: NavController){
    val scale = remember {
        androidx.compose.animation.core.Animatable(0f)
    }

    // AnimationEffect
    LaunchedEffect(key1 = true) {
        scale.animateTo(
            targetValue = 1.0f,
            animationSpec = tween(
                durationMillis = 1500,
                easing = {
                    OvershootInterpolator(4f).getInterpolation(it)
                })
        )
        delay(3000L)
        navController.navigate(StudentQuizRoutes.Login.route)
    }

    // Image
    Box(contentAlignment = Alignment.TopStart,
        modifier = Modifier.fillMaxSize().padding(start = 20.dp, top = 100.dp)
    ) {
        Image(painter = painterResource(id = R.drawable.student_quiz),
            contentDescription = "Logo",
            modifier = Modifier.scale(scale.value))
    }
}