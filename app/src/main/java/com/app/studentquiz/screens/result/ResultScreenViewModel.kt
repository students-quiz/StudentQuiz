package com.app.studentquiz.screens.result

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import com.app.studentquiz.common.objectclasses.Buff
import com.app.studentquiz.common.objectclasses.Reward
import com.app.studentquiz.screens.questions.QuestionState
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.storage.FirebaseStorage
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ResultScreenViewModel @Inject constructor(

    val db: FirebaseFirestore,
    val storage: FirebaseStorage,
    private val function: FirebaseFunctions
) : ViewModel() {

    val rewardCalcInProgress = mutableStateOf(false)
    val buffCalcInProgress = mutableStateOf(false)
    val bonusCalcInProgress = mutableStateOf(false)
    val resultCalcFailed = mutableStateOf(false)
    val score = mutableStateOf(Score())
    val bonusScore = mutableStateOf(Score())
    val reward = mutableStateOf(Reward())
    val bonusReward = mutableStateOf(Reward())
    val buff = mutableStateOf(Buff())

    fun calcScore(questionState: List<QuestionState>) {
        rewardCalcInProgress.value = true
        buffCalcInProgress.value = true
        bonusCalcInProgress.value = true
        val qs = questionState.map {
            hashMapOf(
                "qid" to it.id,
                "bonus" to (it.questionData.bonusParent.isNotBlank()),
                "correct" to (it.answer == it.questionData.correct_option)
            )
        }
        val data = hashMapOf(
            "questions" to qs
        )
        // remove comment when using local emulator
        // function.useEmulator("10.0.2.2", 5001)
        function
            .getHttpsCallable("score-calcScore")
            .call(data)
            .continueWith { task ->
                if (!task.isSuccessful) {
                    rewardCalcInProgress.value = false
                    buffCalcInProgress.value = false
                    bonusCalcInProgress.value = false
                    resultCalcFailed.value = true
                } else {
                    val result = task.result ?: throw Exception("result is null")
                    val dt = result.data
                    var sc = Score()
                    var bonSc = Score()
                    if (dt is HashMap<*, *>) {
                        val am = dt["reward_amount"]
                        sc = Score(
                            reward = dt["reward"] as String,
                            amount = if (am is Double) am else (am as Int).toDouble(),
                            buff = dt["buff"] as String
                        )
                        fetchBuff(sc.buff)
                        //now fetch bonus reward, only if bonus calculated
                        fetchReward(
                            sc.reward,
                            onSuccess = {
                                //fetch reward Object
                                reward.value = it.toObjects(Reward::class.java).first()
                                rewardCalcInProgress.value = false
                            }
                        )
                        val bon = dt["bonus"]
                        if (bon is HashMap<*, *>) {
                            val bAm = bon["amount"]
                            bonSc = Score(
                                reward = bon["reward"] as String,
                                amount = if (bAm is Double) bAm else (bAm as Int).toDouble(),
                            )
                        }
                        if (bonSc.reward.isNotBlank()) {

                            fetchReward(
                                bonSc.reward,
                                onSuccess = {
                                    //fetch reward Object
                                    bonusReward.value = it.toObjects(Reward::class.java).first()
                                    bonusCalcInProgress.value = false
                                }
                            )
                        } else {
                            bonusCalcInProgress.value = false
                        }
                    }
                    score.value = sc
                    bonusScore.value = bonSc
                }

            }

    }

    /**
     * TODO
     *
     * @param rwd reward String
     * @param onSuccess func to run after success
     */
    private fun fetchReward(
        rwd: String,
        onSuccess: (QuerySnapshot) -> Unit
    ) {
        db.collection("rewards")
            .whereEqualTo("name", rwd)
            .get()
            .addOnSuccessListener { onSuccess(it) }
    }

    private fun fetchBuff(bf: String) {
        db.collection("buffs")
            .whereEqualTo("name", bf)
            .get()
            .addOnSuccessListener {
                buff.value = it.toObjects(Buff::class.java).first()
                buffCalcInProgress.value = false
            }
    }

}