package com.app.studentquiz.screens.students.studentprofile.studenttopics

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.app.studentquiz.repo.users.User
import com.app.studentquiz.screens.categoryselection.TopicsGrid

const val STUDENT_TOPIC = "studentTopic"

@Composable
fun StudentTopicScreen(
    vm: StudentTopicViewModel = hiltViewModel(),
    student: User?

){
    LaunchedEffect(key1 = true) {
        vm.loadStudentTopics(student)
    }

    TopicsGrid(
        modifier = Modifier
            .padding(vertical = 56.dp)
            .wrapContentHeight(),
        leafTags = vm.uiState.leafTags,
        onSelectTag = {}
    )

}