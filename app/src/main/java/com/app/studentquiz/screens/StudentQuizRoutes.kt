package com.app.studentquiz.screens

sealed class StudentQuizRoutes(val route: String) {
    object SplashScreen : StudentQuizRoutes("splashscreen")
    object Login : StudentQuizRoutes("login")
    object Home : StudentQuizRoutes("home")
    object Questions : StudentQuizRoutes("questions")
    object Results : StudentQuizRoutes("results")
    object Profile : StudentQuizRoutes("profile")
    object TeacherSelect : StudentQuizRoutes("teacher")
    object Students : StudentQuizRoutes("students")
    object StudentsProfile : StudentQuizRoutes("studentProfile")
    object StudentsTopics : StudentQuizRoutes("studentTopics")

}