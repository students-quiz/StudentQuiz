package com.app.studentquiz.screens.categoryselection

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import com.app.studentquiz.repo.users.User
import com.app.studentquiz.screens.login.LoginRepository
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

const val TAGS = "tags"

@HiltViewModel
class CategoryViewModel @Inject constructor(

    val db: FirebaseFirestore,
    private val authRepo: LoginRepository,
    private val catRepo: CategoryRepository
) : ViewModel() {

    val leafTags = mutableStateOf(listOf<Category>())
    val selectedTags = mutableStateOf(listOf<String>())


    fun selectTag(cat: Category) {
        if (cat.display_name in selectedTags.value) {
            selectedTags.value = selectedTags.value.minus(cat.display_name)
        } else {
            selectedTags.value = selectedTags.value.plus(cat.display_name)
        }
    }

    fun getLeafTags() {
        if (leafTags.value.isEmpty()) {

            val firebaseUser = authRepo.currentUser()
            firebaseUser?.let {
                catRepo.getLeafTags(
                    firebaseUser.uid
                ){ catList ->
                    leafTags.value = catList
                }

            }


        }

    }
}