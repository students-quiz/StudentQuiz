package com.app.studentquiz.screens.login

import android.util.Log
import com.app.studentquiz.core.Constants
import com.google.android.gms.auth.api.identity.BeginSignInRequest
import com.google.android.gms.auth.api.identity.SignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuth.AuthStateListener
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FieldValue.serverTimestamp
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.tasks.await

import com.app.studentquiz.screens.login.Response.*
import com.google.firebase.auth.FirebaseUser
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class LoginRepository  @Inject constructor(
    val auth: FirebaseAuth,
    private var oneTapClient: SignInClient,
    @Named("SIGN_IN_REQUEST")
    private var signInRequest: BeginSignInRequest,
    @Named("SIGN_UP_REQUEST")
    private var signUpRequest: BeginSignInRequest,
    private var signInClient: GoogleSignInClient,
    private val usersRef: CollectionReference
)  {
    fun isUserAuthenticatedInFirebase() = auth.currentUser != null

    fun currentUser(): FirebaseUser? = auth.currentUser

    suspend fun oneTapSignInWithGoogle() = flow {
        try {
            emit(Loading)
            val result = oneTapClient.beginSignIn(signInRequest).await()
            emit(Success(result))
        } catch (e: Exception) {
            emit(Failure(e))
        }
    }


    suspend fun oneTapSignUpWithGoogle() = flow {
        try {
            emit(Loading)
            val result = oneTapClient.beginSignIn(signUpRequest).await()
            emit(Success(result))
        } catch (e: Exception) {
            emit(Failure(e))
        }
    }

    suspend fun firebaseSignInWithGoogle(googleCredential: AuthCredential) = flow {
        try {
            emit(Loading)
            val authResult = auth.signInWithCredential(googleCredential).await()
            val isNewUser = authResult.additionalUserInfo?.isNewUser
            emit(Success(isNewUser))
        } catch (e: Exception) {
            emit(Failure(e))
        }
    }

    suspend fun firebaseSignInWithEmail(email: String, pass: String) = flow {
        try {
            emit(Loading)
            val task = auth.signInWithEmailAndPassword(email, pass).await()
            val isNewUser = task?.additionalUserInfo?.isNewUser
            emit(Success(isNewUser))
        } catch (e: Exception) {
            emit(Failure(e))
        }
    }


    suspend fun createUserInFirestore() = flow {
        try {
            emit(Loading)
            auth.currentUser?.apply {
                usersRef.document(uid).set(mapOf(
                    Constants.DISPLAY_NAME to displayName,
                    Constants.EMAIL to email,
                    Constants.PHOTOURL to photoUrl,
                    Constants.ROLES to mapOf(
                        Constants.ROLES_ADMIN to false,
                        Constants.ROLES_STUDENT to true
                    )
                )).await()
                emit(Success(true))
            }
        } catch (e: Exception) {
            emit(Failure(e))
        }
    }


    fun getFirebaseAuthState() = callbackFlow  {
        val authStateListener = AuthStateListener { auth ->
            trySend(auth.currentUser == null)
        }
        auth.addAuthStateListener(authStateListener)
        awaitClose {
            auth.removeAuthStateListener(authStateListener)
        }
    }

    suspend fun signOut() = flow {
        try {
            emit(Loading)
            auth.signOut()
            oneTapClient.signOut().await()
            emit(Success(true))
        } catch (e: Exception) {
            emit(Failure(e))
        }
    }
//
//    suspend fun revokeAccess() = flow {
//        try {
//            emit(Loading)
//            auth.currentUser?.apply {
//                usersRef.document(uid).delete().await()
//                delete().await()
//                signInClient.revokeAccess().await()
//                oneTapClient.signOut().await()
//            }
//            emit(Success(true))
//        } catch (e: Exception) {
//            emit(Failure(e))
//        }
//    }

    fun getDisplayName() = auth.currentUser?.displayName.toString()

    fun getPhotoUrl() = auth.currentUser?.photoUrl.toString()
}