package com.app.studentquiz.screens.login

import android.util.Log
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.google.android.gms.auth.api.identity.BeginSignInResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject
import com.app.studentquiz.screens.login.Response.*
import com.google.android.gms.auth.api.identity.SignInClient
import com.google.firebase.auth.AuthCredential
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.coroutines.launch

@HiltViewModel
class LogInViewModel @Inject constructor(
    private val repo: LoginRepository,
    val db: FirebaseFirestore,
    val storage: FirebaseStorage,
    val oneTapClient: SignInClient
) : ViewModel() {

    val isUserAuthenticated get() = repo.isUserAuthenticatedInFirebase()
    val inProgress = mutableStateOf(false)

    private val _oneTapSignInState = mutableStateOf<Response<BeginSignInResult>>(Success(null))
    val oneTapSignInState: State<Response<BeginSignInResult>> = _oneTapSignInState

    private val _oneTapSignUpState = mutableStateOf<Response<BeginSignInResult>>(Success(null))
    val oneTapSignUpState: State<Response<BeginSignInResult>> = _oneTapSignUpState

    private val _signInState = mutableStateOf<Response<Boolean>>(Success(null))
    val signInState: State<Response<Boolean>> = _signInState

    private val _createUserState = mutableStateOf<Response<Boolean>>(Success(null))
    val createUserState: State<Response<Boolean>> = _createUserState

    fun getAuthState() = liveData(Dispatchers.IO) {
        repo.getFirebaseAuthState().collect { response ->
            emit(response)
        }
    }

    fun onLogin(email: String, pass: String, navigate: () -> Unit) {
        if (email.isEmpty() or pass.isEmpty()) {
            return
        }
        inProgress.value = true
        repo.auth.signInWithEmailAndPassword(email, pass)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    inProgress.value = false
                    repo.auth.currentUser?.uid?.let { uid ->

                        Log.d("LOGIN", "LoggedIn....$uid")
                    }
                } else {
                    Log.e("LOGIN", "Login failed")
                    inProgress.value = false
                }
            }
            .addOnFailureListener { exc ->
                Log.e("LOGIN", "Login failed")
                inProgress.value = false
            }
    }


    fun oneTapSignIn() {
        viewModelScope.launch {
            repo.oneTapSignInWithGoogle().collect { response ->
                _oneTapSignInState.value = response
            }
        }
    }

    fun oneTapSignUp() {
        viewModelScope.launch {
            repo.oneTapSignUpWithGoogle().collect { response ->
                _oneTapSignUpState.value = response
            }
        }
    }

    fun signInWithGoogle(googleCredential: AuthCredential) {
        viewModelScope.launch {
            repo.firebaseSignInWithGoogle(googleCredential).collect { response ->
                _signInState.value = response
            }
        }
    }

    fun signInWithEmail(email: String, pass: String) {
        viewModelScope.launch {
            repo.firebaseSignInWithEmail(email, pass).collect { response ->
                _signInState.value = response
            }
        }
    }


    fun createUser() {
        viewModelScope.launch {
            repo.createUserInFirestore().collect { response ->
                _createUserState.value = response
            }
        }
    }

    fun subscribeToFCM(){
        FirebaseMessaging.getInstance().subscribeToTopic("NEWS").addOnSuccessListener {
            Log.d("SUBSCRIPTION", "Successfully subscribed to NEWS")
        }
    }
}