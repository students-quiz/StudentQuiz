package com.app.studentquiz.screens.profile

import com.app.studentquiz.common.objectclasses.Buff
import com.app.studentquiz.common.objectclasses.Reward
import com.app.studentquiz.repo.users.ActiveBuff
import com.app.studentquiz.repo.users.User
import com.google.firebase.Timestamp
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*
import javax.inject.Inject

class ProfileRepository  @Inject constructor(
    val db: FirebaseFirestore,
)
{
    /**
     * gets the userProfile for userId
     * also sets the UIState variables
     *
     * @param userId
     * @param uiState
     * @param onSuccess action to be taken after fetched
     */
    fun fetchUserProfile(
        userId : String,
        uiState: ProfileUIState,
        onSuccess: (User) -> Unit = { }
    ){
        uiState.userFetchInProgress = true
        uiState.buffFetchInProgress = true
        uiState.rewardsFetchInProgress = true
        uiState.teacherFetchInProgress = true
        db.collection("users")
            .document(userId)
            .get()
            .addOnSuccessListener { doc ->
                doc?.let {

                    uiState.userProfile = doc.toObject(User::class.java)!!
                    uiState.userFetchInProgress = false
                    fetchBuffs(uiState)
                    fetchRewards(uiState)
                    fetchTeacher(uiState)
                    onSuccess(uiState.userProfile)
                }
            }
    }

    /**
     * fetch teacher of the user uiState.userProfile
     * and sets the appropriate states of uiState
     *
     * @param uiState
     */
    private fun fetchTeacher(uiState: ProfileUIState) {
        val user = uiState.userProfile
        if (user.teacher.isNotBlank()) {
            db.collection("users")
                .document(user.teacher)
                .get()
                .addOnSuccessListener { doc ->
                    doc?.let {
                        val teacher = doc.toObject(User::class.java)
                        teacher?.let {
                            uiState.teacherProfile = Teacher(
                                uid = doc.id,
                                name = teacher.name,
                                email = teacher.email
                            )
                        }
                    }
                    uiState.teacherFetchInProgress = false

                }
        }else{
            // when there is no teacher assigned let them select teacher
            uiState.teacherFetchInProgress = false
        }
    }

    /**
     * fetch rewards of the user uiState.userProfile
     * and sets the appropriate states of uiState
     *
     * @param uiState
     */
    private fun fetchRewards(uiState: ProfileUIState) {
        val user = uiState.userProfile
        val rewardNames = user.rewards.keys.toList()
        db.collection("rewards")
            .whereIn("name", rewardNames)
            .get()
            .addOnSuccessListener { q ->
                uiState.rewards = q.map { it.toObject(Reward::class.java) }
                uiState.rewardsFetchInProgress = false
            }
    }

    /**
     * fetch buffs of the user uiState.userProfile
     * and sets the appropriate states of uiState
     *
     * @param uiState
     */
    private fun fetchBuffs(uiState: ProfileUIState) {
        val user = uiState.userProfile
        var buffNames = user.buffs.keys.toList()
        user.activeBuff?.let {
            buffNames = buffNames.plus(user.activeBuff.buff)
        }

        db.collection("global_settings")
            .document("active_global_buffs")
            .get()
            .addOnCompleteListener { agb ->
                if (agb.isSuccessful) {
                    val globBuff = agb.result.data

                    globBuff?.let {
                        val buff = globBuff["buff"] as HashMap<*, *>
                        val start = (buff["start"] as Timestamp).toDate()
                        val end = (buff["end"] as Timestamp).toDate()
                        val now = Date()
                        if (now > start && now < end) {
                            buffNames = buffNames.plus(buff["name"] as String)
                            uiState.gBuff = ActiveBuff(
                                buff = buff["name"] as String,
                                expireAt = end.time
                            )
                        }
                    }
                }

                if (buffNames.isNotEmpty()) {
                    db.collection("buffs")
                        .whereIn("name", buffNames)
                        .get()
                        .addOnSuccessListener { q ->
                            uiState.buffs = q.map {
                                it.toObject(Buff::class.java)
                            }
                            uiState.buffFetchInProgress = false
                        }
                } else {
                    uiState.buffFetchInProgress = false
                }
            }


    }
}