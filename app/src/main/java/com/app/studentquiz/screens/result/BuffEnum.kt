package com.app.studentquiz.screens.result

enum class BuffEnum (val value: String) {
    ThreeXchancetoys("3_x_chance_toys"),
    TowXamountfood ("2_x_amount_food") ;

}


 inline fun <reified T : Enum<T>, V> ((T) -> V).find(value: V): T? {
    return enumValues<T>().firstOrNull { this(it) == value }
}