@file:OptIn(ExperimentalMaterialApi::class)

package com.app.studentquiz.screens.students.studentprofile

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.hilt.navigation.compose.hiltViewModel
import com.app.studentquiz.common.component.NetworkImage
import com.app.studentquiz.common.objectclasses.Reward
import com.app.studentquiz.repo.users.User
import com.app.studentquiz.screens.profile.ProfileScreenContent

const val STUDENT_PROFILE = "studentProfile"

@Composable
fun StudentProfileScreen(
    vm: StudentProfileViewModel = hiltViewModel(),
    student: User?
) {
    LaunchedEffect(key1 = true) {
        vm.loadStudentProfile(student)
    }
    // student profile
    ProfileScreenContent(
        signOutOnClick = { },
        userProfile = vm.uiState.userProfile,
        teacher = vm.profileUIState.teacherProfile,
        showTeacher = !vm.profileUIState.teacherFetchInProgress,
        selectTeacherClick = { },
        showUserProfile = !vm.profileUIState.userFetchInProgress,
        activateBuffClick = { },
        buffs = vm.profileUIState.buffs,
        globalBuff = vm.profileUIState.gBuff,
        rewards = vm.profileUIState.rewards,
        showRewards = !vm.profileUIState.rewardsFetchInProgress,
        showBuffs = !vm.profileUIState.buffFetchInProgress,
        onRewardsClick = { rwd ->
            vm.uiState.selectedReward = rwd
            vm.uiState.showDialog = true
        }

    )
    student?.let {
        if (vm.uiState.showDialog) {
            CustomDialog(
                setShowDialog = {
                    vm.uiState.showDialog = it
                },
                selectedReward = vm.uiState.selectedReward,
                student = student,
                updateRewardOnClick = {
                    vm.updateRewardAmount(it, student)
                    // Close dialog after update
                    vm.uiState.showDialog = false
                }
            )
        }
    }


}

/**
 * custom dialog
 *
 * @param setShowDialog
 * @param selectedReward
 * @param student
 * @param valueChange
 */
@Composable
fun CustomDialog(
    setShowDialog: (Boolean) -> Unit,
    selectedReward: Reward,
    student: User,
    updateRewardOnClick: (String) -> Unit
) {

    var valueChangeState by remember { mutableStateOf("") }
    Dialog(onDismissRequest = { setShowDialog(false) }) {
        val userReward = student.rewards[selectedReward.name]
        Surface(
            shape = MaterialTheme.shapes.medium,
            color = MaterialTheme.colors.surface
        ) {
            Column(
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    text = "Update ${selectedReward.name}",
                    style = MaterialTheme.typography.h1,
                    modifier = Modifier.padding(bottom = 16.dp)
                )
                Row {
                    BadgeBox(
                        badgeContent = {
                            Text(
                                text = userReward.toString(),
                                fontSize = 8.sp
                            )
                        },
                        modifier = Modifier
                            .padding(16.dp)
                    ) {

                        Box {
                            NetworkImage(
                                url = selectedReward.image_url,
                                contentDescription = selectedReward.name,
                                modifier = Modifier
                                    .size(width = 40.dp, height = 40.dp)
                                    .fillMaxSize(),
                                contentScale = ContentScale.Fit
                            )
                        }
                    }
                    TextField(value = valueChangeState, onValueChange = {
                        valueChangeState = it
                    })
                }
                Button(onClick = { updateRewardOnClick(valueChangeState) }) {
                    Text(text = "UPDATE")
                }
            }


        }
    }
}