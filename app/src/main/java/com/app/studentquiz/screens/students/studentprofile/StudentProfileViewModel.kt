package com.app.studentquiz.screens.students.studentprofile

import androidx.compose.runtime.Stable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import com.app.studentquiz.common.objectclasses.Reward
import com.app.studentquiz.repo.users.User
import com.app.studentquiz.screens.profile.ProfileRepository
import com.app.studentquiz.screens.profile.ProfileUIState
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class StudentProfileViewModel @Inject constructor(
    private val profileRepo: ProfileRepository,
    private val db: FirebaseFirestore
) : ViewModel() {

    val profileUIState = ProfileUIState()
    val uiState = UIState()
    fun loadStudentProfile(student: User?) {
        student?.let { st ->
            profileRepo
                .fetchUserProfile(
                    st.uid,
                    profileUIState,
                    onSuccess = { usr ->
                        uiState.userProfile = usr
                    }
                )

        }
    }

    fun updateRewardAmount(
        amtStr: String,
        student: User?
    ) {
        student?.let { st ->
            amtStr.toDoubleOrNull()?.let { dVal ->
                val newUser = uiState.userProfile.copy()
                val rewards = newUser.rewards
                rewards[uiState.selectedReward.name]?.let {
                    rewards[uiState.selectedReward.name] = it + dVal
                }
                val data = hashMapOf("rewards" to rewards)
                db.collection("users")
                    .document(st.uid)
                    .set(data, SetOptions.merge() )
                    .addOnSuccessListener {
                        uiState.userProfile = newUser
                    }
            }
        }

    }
}


/**
 * keeps all the variables for ui state
 * @ stable so that change in the variables makes recomposition
 */
@Stable
class UIState {
    var selectedReward by mutableStateOf(Reward())
    var showDialog by mutableStateOf(false)
    var userProfile by mutableStateOf(User())
}

