package com.app.studentquiz.screens.profile

import androidx.compose.runtime.State
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.studentquiz.screens.login.LoginRepository
import com.app.studentquiz.screens.login.Response
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.functions.FirebaseFunctions
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProfileScreenViewModel @Inject constructor(

    private val authRepo: LoginRepository,
    private val function: FirebaseFunctions,
    private val profileRepo: ProfileRepository
) : ViewModel() {

    private val _signOutState = mutableStateOf<Response<Boolean>>(Response.Success(false))
    val signOutState: State<Response<Boolean>> = _signOutState
    var uiState by mutableStateOf(ProfileUIState())

    fun signOut() {
        viewModelScope.launch {
            authRepo.signOut().collect { response ->
                _signOutState.value = response
            }
        }
    }

    fun fetchUser() {
        val firebaseUser = authRepo.currentUser()
        firebaseUser?.let {
            profileRepo.fetchUserProfile(firebaseUser.uid, uiState)
        }
    }


    fun activateBuff(buff: String = "") {
        val data = hashMapOf(
            "buffName" to buff
        )

        function
            .getHttpsCallable("score-activateBuff")
            .call(data)
            .continueWith { task ->
                task.result ?: throw Exception("result is null")

            }
    }

}


