@file:OptIn(ExperimentalMaterialApi::class)

package com.app.studentquiz.screens.profile

import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.app.studentquiz.common.objectclasses.Buff
import com.app.studentquiz.common.objectclasses.Reward
import com.app.studentquiz.repo.users.ActiveBuff
import com.app.studentquiz.repo.users.User
import com.app.studentquiz.screens.StudentQuizRoutes
import com.app.studentquiz.screens.login.ProgressBar
import com.app.studentquiz.screens.login.Response

const val CURRENT_TEACHER = "current_teacher"

@Composable
fun ProfileScreen(
    viewModel: ProfileScreenViewModel = hiltViewModel(),
    navController: NavController
) {
    LaunchedEffect(key1 = Unit) {
        viewModel.fetchUser()
    }
    // profile screen content
    ProfileScreenContent(
        signOutOnClick = { viewModel.signOut() },
        userProfile = viewModel.uiState.userProfile,
        teacher = viewModel.uiState.teacherProfile,
        showTeacher = !viewModel.uiState.teacherFetchInProgress,
        selectTeacherClick = {
            // send current teacher name to teacher selection
            navController.currentBackStackEntry?.savedStateHandle?.set(
                CURRENT_TEACHER,
                viewModel.uiState.teacherProfile.name
            )
            navController.navigate(StudentQuizRoutes.TeacherSelect.route)
        },
        showUserProfile = !viewModel.uiState.userFetchInProgress,
        activateBuffClick = viewModel::activateBuff,
        buffs = viewModel.uiState.buffs,
        globalBuff = viewModel.uiState.gBuff,
        rewards = viewModel.uiState.rewards,
        showRewards = !viewModel.uiState.rewardsFetchInProgress,
        showBuffs = !viewModel.uiState.buffFetchInProgress,
        onRewardsClick = { } // for students no rewardClick event
    )


    when (val signOutResponse = viewModel.signOutState.value) {
        is Response.Loading -> ProgressBar()
        is Response.Success -> {
            val signedOut = signOutResponse.data
            signedOut?.let {
                if (signedOut) {
                    LaunchedEffect(signOutResponse.data) {
                        navController.navigate(StudentQuizRoutes.Login.route)
                    }
                }
            }
        }
        is Response.Failure -> signOutResponse.e?.let {
            LaunchedEffect(Unit) {
                print(it)
            }
        }
    }
}


@Preview(showBackground = true)
@Composable
fun ProfileScreenPreview() {
    val user = User(
        name = "Arnab Senapati",
        email = "senapati.arnab@gmail.com",
        activeBuff = ActiveBuff(
            buff = "3_x_chance_toys",
            expireAt = 1657169463000
        ),
        buffs = hashMapOf(
            "3_x_chance_toys" to 3,
            "2_x_amount_food" to 100,
            "5_x_amount_food" to 100,
            "6_x_amount_food" to 100,
            "7_x_amount_food" to 100,
            "8_x_amount_food" to 100
        ),
        rewards = hashMapOf(
            "Toys" to 3.25,
            "Food" to 10.5

        )

    )
    val buffs = listOf(
        Buff(
            name = "3_x_chance_toys",
            displayname = "3XCT",
            image_url = "https://firebasestorage.googleapis.com/v0/b/studentquiz-d86f5.appspot.com/o/buff-reward-images%2F2_X_amount_food.png?alt=media&token=b3ded7cc-f72a-4d3e-bef6-6a2e95bc827d"
        ),

        Buff(
            name = "2_x_amount_food",
            displayname = "2XAF",
            image_url = "https://firebasestorage.googleapis.com/v0/b/studentquiz-d86f5.appspot.com/o/buff-reward-images%2F2_X_amount_food.png?alt=media&token=b3ded7cc-f72a-4d3e-bef6-6a2e95bc827d"
        ),

        Buff(
            name = "5_x_amount_food",
            displayname = "5XAF",
            image_url = "https://firebasestorage.googleapis.com/v0/b/studentquiz-d86f5.appspot.com/o/buff-reward-images%2F2_X_amount_food.png?alt=media&token=b3ded7cc-f72a-4d3e-bef6-6a2e95bc827d"
        ),

        Buff(
            name = "6_x_amount_food",
            displayname = "6XAF",
            image_url = "https://firebasestorage.googleapis.com/v0/b/studentquiz-d86f5.appspot.com/o/buff-reward-images%2F2_X_amount_food.png?alt=media&token=b3ded7cc-f72a-4d3e-bef6-6a2e95bc827d"
        ),

        Buff(
            name = "7_x_amount_food",
            displayname = "7XAF",
            image_url = "https://firebasestorage.googleapis.com/v0/b/studentquiz-d86f5.appspot.com/o/buff-reward-images%2F2_X_amount_food.png?alt=media&token=b3ded7cc-f72a-4d3e-bef6-6a2e95bc827d"
        ),

        Buff(
            name = "8_x_amount_food",
            displayname = "8XAF",
            image_url = "https://firebasestorage.googleapis.com/v0/b/studentquiz-d86f5.appspot.com/o/buff-reward-images%2F2_X_amount_food.png?alt=media&token=b3ded7cc-f72a-4d3e-bef6-6a2e95bc827d"
        )
    )
    val rewards = listOf(
        Reward(
            name = "Toys",
            image_url = "https://firebasestorage.googleapis.com/v0/b/studentquiz-d86f5.appspot.com/o/buff-reward-images%2Frwd_Study.png?alt=media&token=91042f6a-96e0-41a9-98fd-f71c2690457b"
        ),
        Reward(
            name = "Food",
            image_url = "https://firebasestorage.googleapis.com/v0/b/studentquiz-d86f5.appspot.com/o/buff-reward-images%2Frwd_Study.png?alt=media&token=91042f6a-96e0-41a9-98fd-f71c2690457b"
        )
    )
    ProfileScreenContent(
        signOutOnClick = { },
        userProfile = user,
        teacher = Teacher(
            name = ""
        ),
        showTeacher = true,
        selectTeacherClick = {},
        true,
        activateBuffClick = {},
        buffs = buffs,
        rewards = rewards,
        globalBuff = ActiveBuff(
            buff = "8_x_amount_food",
            expireAt = 1657169463000
        ),
        showRewards = true,
        showBuffs = true,
        onRewardsClick = {}
    )
}
