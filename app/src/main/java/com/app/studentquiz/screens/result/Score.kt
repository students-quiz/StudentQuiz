package com.app.studentquiz.screens.result

import kotlinx.serialization.Serializable

@Serializable
data class Score(
    val reward: String = "",
    val amount: Double = 0.0,
    val buff: String = "",
)


/*
"reward" -> "Toys"
"reward_amount" -> {Double@17150} 0.02
"correctAnswer" -> {Double@17152} 0.25
"buff" -> "3_x_chance_toys"*/
