package com.app.studentquiz.screens

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.layout.Column
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.compose.currentBackStackEntryAsState

val items = listOf(
    BottomNavItem(
        name = "Home",
        route = StudentQuizRoutes.Home.route,
        icon = Icons.Default.Home
    ),
    BottomNavItem(
        name = "Profile",
        route = StudentQuizRoutes.Profile.route,
        icon = Icons.Default.AccountBox
    ),
    BottomNavItem(
        name = "Students",
        route = StudentQuizRoutes.Students.route,
        icon = Icons.Default.SupervisedUserCircle,
        visibleForStudent = false,
    ),
)

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun BottomNavigationBar(
    navController: NavController,
    modifier: Modifier = Modifier,
    vm: BottomNavViewModel = hiltViewModel()
) {
    LaunchedEffect(key1 = Unit) {
        // if current user has teacher role
        vm.checkRole()
    }
    val bottomBarState = remember { mutableStateOf(true) }
    val backStackEntry = navController.currentBackStackEntryAsState()
    bottomBarState.value = backStackEntry.value?.destination?.route?.let { bottomNavigationState(route = it) } == true
    AnimatedVisibility(
        visible = bottomBarState.value,
        enter = slideInVertically(initialOffsetY = { it }),
        exit = slideOutVertically(targetOffsetY = { it }),
        content = {
            BottomNavigation(
                modifier = modifier,
                backgroundColor = Color.DarkGray,
                elevation = 5.dp
            ) {
                items
                    .filter {
                        // all navitems are visible if teacher
                        // for students only if the item should be shown to students
                        vm.isTeacher || it.visibleForStudent
                    }
                    .forEach { item ->
                    val selected = item.route == backStackEntry.value?.destination?.route
                    BottomNavigationItem(
                        selected = selected,
                        onClick = { navController.navigate(item.route) },
                        selectedContentColor = Color.Green,
                        unselectedContentColor = Color.Gray,
                        icon = {
                            Column(horizontalAlignment = Alignment.CenterHorizontally) {
                                if (item.badgeCount > 0) {
                                    BadgeBox(
                                        badgeContent = {
                                            Text(text = item.badgeCount.toString())
                                        }
                                    ) {
                                        Icon(
                                            imageVector = item.icon,
                                            contentDescription = item.name
                                        )
                                    }
                                } else {
                                    Icon(
                                        imageVector = item.icon,
                                        contentDescription = item.name
                                    )
                                }
                                if (selected) {
                                    Text(
                                        text = item.name,
                                        textAlign = TextAlign.Center,
                                        fontSize = 10.sp
                                    )
                                }
                            }
                        }
                    )
                }
            }

        }
    )
}


@Composable
fun bottomNavigationState(route: String): Boolean {
    var visible = false
    when (route) {// routes where bottom navigation will be shown
        StudentQuizRoutes.Home.route,
        StudentQuizRoutes.Results.route,
        StudentQuizRoutes.Profile.route,
        StudentQuizRoutes.TeacherSelect.route,
        StudentQuizRoutes.Students.route,
        StudentQuizRoutes.StudentsProfile.route,
        StudentQuizRoutes.StudentsTopics.route         -> {
            visible = true
        }
    }
    return visible
}
