package com.app.studentquiz.screens

import android.app.Activity
import android.util.Log
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.IntentSenderRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.app.studentquiz.screens.login.LogInViewModel
import com.app.studentquiz.R
import com.app.studentquiz.core.Constants
import com.app.studentquiz.screens.login.GoogleSignInButton
import com.app.studentquiz.screens.login.ProgressBar
import com.google.android.gms.auth.api.identity.BeginSignInResult
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.GoogleAuthProvider
import com.app.studentquiz.screens.login.Response.*

@Composable
fun LogInScreen(navController: NavController, vm: LogInViewModel) {

    CheckSignedIn(vm = vm, navController = navController)
    Surface {
        Image(
            painter = painterResource(id = R.drawable.signinbg),
            contentDescription = "Background",
            Modifier.fillMaxWidth(),
            contentScale = ContentScale.FillBounds
        )

        Box(
            contentAlignment = Alignment.BottomCenter,
            modifier = Modifier
                .fillMaxSize()
                .padding(start = 20.dp, bottom = 200.dp)
        ) {
            Column {

                val emailState = remember { mutableStateOf(TextFieldValue()) }
                val passState = remember { mutableStateOf(TextFieldValue()) }
                OutlinedTextField(
                    value = emailState.value,
                    onValueChange = { emailState.value = it },
                    modifier = Modifier.padding(8.dp),
                    label = { Text(text = "Email") })
                OutlinedTextField(
                    value = passState.value,
                    onValueChange = { passState.value = it },
                    modifier = Modifier.padding(8.dp),
                    label = { Text(text = "Password") },
                    visualTransformation = PasswordVisualTransformation()
                )
                Button(
                    onClick = {
                        vm.signInWithEmail(
                            emailState.value.text,
                            passState.value.text
                        )
                    },

                    modifier = Modifier.padding(8.dp),
                ) {
                    Row(
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.SpaceAround,
                        modifier = Modifier
                            .width(100.dp)
                            .padding(8.dp)

                    ) {

                        Icon(
                            painter = painterResource(id = R.drawable.ic_baseline_login_24),
                            contentDescription = "LogInIcon"
                        )
                        Text(
                            text = "Log In"
                        )
                    }
                }

                GoogleSignInButton(
                    modifier = Modifier.padding(8.dp),
                    onClick = { vm.oneTapSignIn() }
                )

            }

        }
    }

    val launcher =
        rememberLauncherForActivityResult(ActivityResultContracts.StartIntentSenderForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                try {
                    val credentials = vm.oneTapClient.getSignInCredentialFromIntent(result.data)
                    val googleIdToken = credentials.googleIdToken
                    val googleCredentials = GoogleAuthProvider.getCredential(googleIdToken, null)
                    vm.signInWithGoogle(googleCredentials)
                } catch (it: ApiException) {
                    print(it)
                }
            }
        }


    fun launch(signInResult: BeginSignInResult) {
        val intent = IntentSenderRequest.Builder(signInResult.pendingIntent.intentSender).build()
        launcher.launch(intent)
    }

    when (val oneTapSignInResponse = vm.oneTapSignInState.value) {
        is Loading -> ProgressBar()
        is Success -> {
            oneTapSignInResponse.data?.let {
                LaunchedEffect(it) {
                    launch(it)
                }
            }
        }
        is Failure -> {
            oneTapSignInResponse.e?.let {
                LaunchedEffect(Unit) {
                    print(it)
                    if (it.message == Constants.SIGN_IN_ERROR_MESSAGE) {
                        vm.oneTapSignUp()
                    }
                }
            }
        }
    }

    when (val oneTapSignUpResponse = vm.oneTapSignUpState.value) {
        is Loading -> ProgressBar()
        is Success -> {
            oneTapSignUpResponse.data?.let {
                LaunchedEffect(it) {
                    launch(it)
                }
            }
        }
        is Failure -> oneTapSignUpResponse.e?.let {
            LaunchedEffect(Unit) {
                print(it)
            }
        }
    }

    when (val signInResponse = vm.signInState.value) {
        is Loading -> ProgressBar()
        is Success -> {
            signInResponse.data?.let { isNewUser ->
                if (isNewUser) {
                    LaunchedEffect(isNewUser) {
                        vm.createUser()
                    }
                }else{
                    Log.d(Constants.TAG_SIGN_IN_GOOGLE, "Signed in Successfully")
                    navController.navigate(StudentQuizRoutes.Home.route)
                }
            }
        }
        is Failure -> signInResponse.e?.let {
            LaunchedEffect(Unit) {
                print(it)
            }
        }
    }


    when(val createUserResponse = vm.createUserState.value) {
        is Loading -> ProgressBar()
        is Success -> {
            createUserResponse.data?.let { isUserCreated ->
                if (isUserCreated) {
                    navController.navigate(StudentQuizRoutes.Home.route)
                }
            }
        }
        is Failure -> createUserResponse.e?.let {
            LaunchedEffect(Unit) {
                print(it)
            }
        }
    }
}

@Composable
fun CheckSignedIn(vm: LogInViewModel, navController: NavController) {
    val alreadyLoggedIn = remember { mutableStateOf(false) }
    vm.subscribeToFCM()
    val signedIn = vm.isUserAuthenticated
    if (signedIn && !alreadyLoggedIn.value) {
        alreadyLoggedIn.value = true
        navController.navigate(StudentQuizRoutes.Home.route) {
            popUpTo(0)
        }
    }
}