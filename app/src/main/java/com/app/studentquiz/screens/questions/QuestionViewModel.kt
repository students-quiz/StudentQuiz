package com.app.studentquiz.screens.questions

import android.widget.Toast
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.ViewModel
import com.app.studentquiz.screens.categoryselection.VIDEO_CAT
import com.app.studentquiz.screens.login.LoginRepository
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.storage.FirebaseStorage
import dagger.hilt.android.lifecycle.HiltViewModel
import java.util.*
import javax.inject.Inject
import kotlin.math.min

const val QUESTION_COLLECTION = "questions"
const val VID_Q_LIMIT = 1
const val NORMAL_Q_LIMIT = 10

@HiltViewModel
class QuestionViewModel @Inject constructor(

    val db: FirebaseFirestore,
    val storage: FirebaseStorage,
    private val authRepo: LoginRepository,
) : ViewModel() {

    val questions = mutableStateOf<List<QuestionState>>(listOf<QuestionState>())
    val questionsLoaded = mutableStateOf(false)
    val showingOldQuestions = mutableStateOf(false)

    var currentQuestionIndex = mutableStateOf(0)

    fun loadQuestions(selectedCategories: List<String>?) {
        selectedCategories?.let { lst ->
            val selTags = mutableListOf<String>()
            selTags.addAll(lst)
            val firebaseUser = authRepo.currentUser()
            firebaseUser?.let {
                db.collection("users")
                    .document(firebaseUser.uid)
                    .collection("q_history")
                    .get()
                    .addOnSuccessListener { snap ->
                        //map of doc hist attempt count
                        val qHistMap = mutableMapOf<String, Long>()
                        snap.forEach{ d ->
                            qHistMap[d.id] = d.data["count"] as Long
                        }




                        // taskList in to merge result of multiple queries
                        val taskList = mutableListOf<Task<QuerySnapshot>>()
                        val tsk = db.collection(QUESTION_COLLECTION)
                            .whereArrayContainsAny("tags", selTags)
                            .limit(1000)
                            .get()
                        taskList.add(tsk)
                        if (selTags.contains(VIDEO_CAT) && selTags.size == 1) {
                            // if only video is selected add all questions with video
                            val tskVid = db.collection(QUESTION_COLLECTION)
                                .whereGreaterThan("yt_video_id", "")
                                .limit(1000)
                                .get()
                            taskList.add(tskVid)
                        }
                        val allTasks = Tasks.whenAllSuccess<QuerySnapshot>(taskList)
                        allTasks.addOnSuccessListener { lstQs ->
                            //when allTasks in @taskList is success
                            val docs = mutableListOf<DocumentSnapshot>()
                            lstQs.forEach { docs.addAll(it.documents) }


                            // remove duplicates in case two queries return same questions
                            var distinctDocs = docs.distinctBy { it.id }

                            if (selTags.contains(VIDEO_CAT) ) {
                                //when video tag is selected select only video questions
                                val vidDocs = distinctDocs.filter { s ->
                                    (s.data?.get("yt_video_id") != null && (s.data?.get("yt_video_id") as String).isNotBlank())
                                }
                                if(vidDocs.isNotEmpty()){
                                    distinctDocs = vidDocs
                                }
                            }

                            // sort the docs on count of times attempted in the past
                            val docCnt =  distinctDocs.map { d ->
                                QDocCount(
                                    doc = d,
                                    count = qHistMap[d.id] ?: 0
                                )
                            }
                            val docCntSorted = docCnt.sortedBy { it.count }
                            distinctDocs = docCntSorted.map { it.doc }

                            // select only new questions and non-bonus questions
                            var newQuestions = distinctDocs.filter { s ->
                                (s.data?.get("bonusParent") == null || (s.data?.get("bonusParent") as String).isBlank()) &&
                                        !qHistMap.containsKey(s.id)
                            }

                            if (selTags.contains(VIDEO_CAT) ) {
                                //when video tag is selected select only video questions
                                newQuestions = newQuestions.filter { s ->
                                    (s.data?.get("yt_video_id") != null && (s.data?.get("yt_video_id") as String).isNotBlank())
                                }
                            }else{
                                // when video tag is not selected dont select any video
                                newQuestions = newQuestions.filter { s ->
                                    (s.data?.get("yt_video_id") == null || (s.data?.get("yt_video_id") as String).isBlank())
                                }
                            }

                            val filteredQs = mutableListOf<DocumentSnapshot>()
                            if (newQuestions.isNotEmpty()) {
                                filteredQs.addAll(newQuestions)
                            } else {
                                // if no new questions available show old questions
                                showingOldQuestions.value = true
                                filteredQs.addAll(distinctDocs.filter { s ->
                                    (s.data?.get("bonusParent") == null || (s.data?.get("bonusParent") as String).isBlank())
                                })
                            }


                            val limit = if(selTags.contains(VIDEO_CAT)) VID_Q_LIMIT else NORMAL_Q_LIMIT
                            val shuffledDoc = filteredQs.subList(0, min(limit, filteredQs.size)).shuffled()
                            val qList: List<QuestionState> =
                                shuffledDoc.mapIndexed { index, i ->
                                    val question =
                                        jumbleOptions(i.toObject(Question::class.java)!!)
                                    val showPrevious = index > 0
                                    val showDone = index == shuffledDoc.size - 1
                                    QuestionState(
                                        id = i.id,
                                        questionData = question,
                                        questionIndex = index,
                                        totalQuestionsCount = shuffledDoc.size,
                                        showPrevious = showPrevious,
                                        showDone = showDone
                                    )
                                }


                            questions.value = qList
                            questionsLoaded.value = true
                            currentQuestionIndex.value = 0
                        }

                        db.collection(QUESTION_COLLECTION)
                            .whereArrayContainsAny("tags", selTags).limit(1000)
                            .get()
                            .addOnSuccessListener {

                            }
                    }
            }

        }
    }

    fun addBonusQs(qid: String) {
        questionsLoaded.value = false
        db.collection(QUESTION_COLLECTION)
            .whereEqualTo("bonusParent", qid)
            .get()
            .addOnSuccessListener {
                if (it.documents.size > 0) {
                    val totQCount = questions.value.size + it.documents.size
                    val qList: List<QuestionState> = it.documents.mapIndexed { index, i ->
                        val question = jumbleOptions(i.toObject(Question::class.java)!!)
                        val showPrevious = true
                        val showDone = index == it.documents.size - 1
                        val qIndex = questions.value.size + index
                        QuestionState(
                            id = i.id,
                            questionData = question,
                            questionIndex = qIndex,
                            totalQuestionsCount = totQCount,
                            showPrevious = showPrevious,
                            showDone = showDone
                        )
                    }
                    questions.value.forEach { it ->
                        it.showDone = false
                        it.totalQuestionsCount = totQCount
                    }

                    val qListAdded = questions.value.plus(qList)

                    questions.value = qListAdded
                    questionsLoaded.value = true
                } else {
                    questionsLoaded.value = true
                }


            }
    }

    fun jumbleOptions(question: Question): Question {
        val correct_opt = question.options[question.correct_option]
        question.options = question.options.shuffled().filter { it -> it.isNotBlank() }
        question.correct_option = question.options.indexOf(correct_opt)

        return question
    }


}

data class QDocCount(
    val doc:DocumentSnapshot,
    val count: Long,
)