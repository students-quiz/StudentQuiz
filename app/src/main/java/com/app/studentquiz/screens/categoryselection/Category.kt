package com.app.studentquiz.screens.categoryselection

data class Category(
    val display_name: String = "",
    val leaf: Boolean = false,
    val count: Int = 0,
    var userCount: Int = 0
)
