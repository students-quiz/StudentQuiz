package com.app.studentquiz.repo.users

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

// without Parcelize cannot be passed in navigation
@Parcelize
data class User(
    var uid: String = "",
    val name: String = "",
    val email: String = "",
    val buffs: HashMap<String, Int> = hashMapOf(),
    val tagscount: HashMap<String, Long> = hashMapOf(),
    val rewards: HashMap<String, Double> = hashMapOf(),
    val activeBuff: ActiveBuff? = null,
    val roles: HashMap<String, Boolean> = hashMapOf(),
    val teacher: String = ""
) : Parcelable

// inner objects of User class, needs to be parcelized
@Parcelize
data class ActiveBuff(
    val buff: String = "",
    val expireAt: Long = 0

): Parcelable


