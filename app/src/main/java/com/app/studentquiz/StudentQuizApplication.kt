package com.app.studentquiz

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class StudentQuizApplication : Application() {
}