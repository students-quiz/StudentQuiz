package com.app.studentquiz

import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class FCMService : FirebaseMessagingService() {
    override fun onNewToken(token: String) {
        Log.d("FCM_MSG", "$token")
    }

    override fun onMessageReceived(message: RemoteMessage) {
        Log.d("FCM_MSG", message.notification?.body.toString())
    }
}