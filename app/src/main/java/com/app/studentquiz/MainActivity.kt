package com.app.studentquiz

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.app.studentquiz.screens.BottomNavigationBar
import com.app.studentquiz.screens.StudentQuizRoutes
import com.app.studentquiz.screens.login.LogInViewModel
import com.app.studentquiz.ui.theme.StudentQuizAndroidTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    private lateinit var navController: NavHostController
    private val viewModel by viewModels<LogInViewModel>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            StudentQuizAndroidTheme {

                navController = rememberNavController()
                Scaffold(
                    bottomBar = { BottomNavigationBar(navController = navController) }
                ) {
                    StudentQuizNav(navController = navController)
                }

                checkAuthStatus()
                getAuthState()
            }
        }
    }

    private fun checkAuthStatus() {
        if (viewModel.isUserAuthenticated) {
            navController.findDestination(StudentQuizRoutes.Home.route)
                ?.let { navController.navigate(StudentQuizRoutes.Home.route) }
        }
    }

    private fun getAuthState() = viewModel.getAuthState()
}
