package com.app.studentquiz

import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.app.studentquiz.repo.users.User
import com.app.studentquiz.screens.login.LogInViewModel
import com.app.studentquiz.screens.LogInScreen
import com.app.studentquiz.screens.profile.ProfileScreen
import com.app.studentquiz.screens.QuizSplashScreen
import com.app.studentquiz.screens.StudentQuizRoutes
import com.app.studentquiz.screens.categoryselection.CatSelection
import com.app.studentquiz.screens.categoryselection.CategoryViewModel
import com.app.studentquiz.screens.categoryselection.SELECTED_CATEGORIES
import com.app.studentquiz.screens.profile.CURRENT_TEACHER
import com.app.studentquiz.screens.questions.QUESTION_ANSWER
import com.app.studentquiz.screens.questions.QuestionState
import com.app.studentquiz.screens.questions.QuestionViewModel
import com.app.studentquiz.screens.questions.QuestionsScreen
import com.app.studentquiz.screens.result.ResultScreen
import com.app.studentquiz.screens.result.ResultScreenViewModel
import com.app.studentquiz.screens.students.StudentsScreen
import com.app.studentquiz.screens.students.studentprofile.STUDENT_PROFILE
import com.app.studentquiz.screens.students.studentprofile.StudentProfileScreen
import com.app.studentquiz.screens.students.studentprofile.studenttopics.STUDENT_TOPIC
import com.app.studentquiz.screens.students.studentprofile.studenttopics.StudentTopicScreen
import com.app.studentquiz.screens.teacher.TeacherSelectScreen

@Composable
fun StudentQuizNav(
    navController: NavHostController
){
    val vmLogin = hiltViewModel<LogInViewModel>()
    val vmCategory = hiltViewModel<CategoryViewModel>()
    val vmQuestion = hiltViewModel<QuestionViewModel>()
    val resultScreenViewModel = hiltViewModel<ResultScreenViewModel>()

    NavHost(
        navController = navController,
        startDestination = StudentQuizRoutes.SplashScreen.route
    ) {
        composable(StudentQuizRoutes.SplashScreen.route) {
            QuizSplashScreen(navController = navController)
        }
        composable(StudentQuizRoutes.Login.route) {
            LogInScreen(navController = navController, vm = vmLogin)
        }
        composable(StudentQuizRoutes.Home.route) {
            CatSelection(vm = vmCategory, navController = navController)
        }
        composable(StudentQuizRoutes.Profile.route) {
            ProfileScreen(navController = navController)
        }
        composable(StudentQuizRoutes.TeacherSelect.route) {
            val curTeacher =
                navController.previousBackStackEntry?.savedStateHandle?.get<String>(
                    CURRENT_TEACHER)
            TeacherSelectScreen(
                curTeacher = curTeacher,
                navController = navController
            )
        }
        composable(StudentQuizRoutes.Questions.route) {
            val selectCat =
                navController.previousBackStackEntry?.savedStateHandle?.get<List<String>>(
                    SELECTED_CATEGORIES)

            QuestionsScreen(vm = vmQuestion,navController = navController, selectedCategories = selectCat)
        }
        composable(StudentQuizRoutes.Results.route) {
            val selectCat =
                navController.previousBackStackEntry?.savedStateHandle?.get<List<QuestionState>>(
                    QUESTION_ANSWER
                )

            ResultScreen(questionState = selectCat, vm = resultScreenViewModel, navController)
        }

        composable(StudentQuizRoutes.Students.route) {
            StudentsScreen(navController = navController)
        }

        composable(StudentQuizRoutes.StudentsProfile.route) {
            //send parcelize student to student profile screen
            val student =
                navController.previousBackStackEntry?.savedStateHandle?.get<User>(
                    STUDENT_PROFILE
                )

            StudentProfileScreen(student = student)
        }

        composable(StudentQuizRoutes.StudentsTopics.route) {
            //send parcelize student to student profile screen
            val student =
                navController.previousBackStackEntry?.savedStateHandle?.get<User>(
                    STUDENT_TOPIC
                )

            StudentTopicScreen(student = student)
        }
    }



}